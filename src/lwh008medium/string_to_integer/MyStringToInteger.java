package lwh008medium.string_to_integer;

/*
Implement atoi to convert a string to an integer.

Hint: Carefully consider all possible input cases. If you want a challenge, 
please do not see below and ask yourself what are the possible input cases.

Notes: It is intended for this problem to be specified vaguely (ie, no given input specs). 
You are responsible to gather all the input requirements up front.

Update (2015-02-10):
The signature of the C++ function had been updated. If you still see your function signature 
accepts a const char * argument, please click the reload button  to reset your code definition.

spoilers alert... click to show requirements for atoi.

Requirements for atoi:
The function first discards as many whitespace characters as necessary until the first 
non-whitespace character is found. Then, starting from this character, takes an optional 
initial plus or minus sign followed by as many numerical digits as possible, and interprets 
them as a numerical value.

The string can contain additional characters after those that form the integral number, 
which are ignored and have no effect on the behavior of this function.

If the first sequence of non-whitespace characters in str is not a valid integral number, 
or if no such sequence exists because either str is empty or it contains only whitespace 
characters, no conversion is performed.

If no valid conversion could be performed, a zero value is returned. If the correct value 
is out of the range of representable values, INT_MAX (2147483647) or INT_MIN (-2147483648) 
is returned
 */
public class MyStringToInteger {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		MyStringToInteger sti=new MyStringToInteger();
		
		String inputStr="1";

		int result=sti.stringToInteger(inputStr);	
		System.out.println(result);
		
//		char c='0';
//		int i=c+2;
//		System.out.println(c+" "+i);//0 48
	}

	/*
字符串题一般考查的都是边界条件、特殊情况的处理。所以遇到此题一定要问清楚各种条件下的
输入输出应该是什么样的。这里已知的特殊情况有：

能够排除首部的空格，从第一个非空字符开始计算
允许数字以正负号(+-)开头
遇到非法字符便停止转换，返回当前已经转换的值，如果开头就是非法字符则返回0
在转换结果溢出时返回特定值，这里是最大/最小整数


注意：计算的时候可以直接使用char来计算
	 */
	/**
	 * @param inputStr
	 * @return
	 */
	private int stringToInteger(String str) {
		
		String processedStr=str.trim();
		int length=processedStr.length();
		long intResult=0;
		boolean isPositive=true;
		
		for(int i=0;i<length;i++){
			char c=processedStr.charAt(i);
			if(i==0&&(c=='-'||c=='+')){
				isPositive=c=='+'?true:false;
			}else if(c>='0'&&c<='9'){	//if not example 4 go to else return 0
				//char '0'==0
				if(intResult>(Integer.MAX_VALUE-(c-'0'))/10){	// all statement is to ensure 10*intResult+c-'0'<Integer.MAX_VALUE
					return isPositive?Integer.MAX_VALUE:Integer.MIN_VALUE;
				}	
				intResult=intResult*10;
				intResult+=c-'0';	//如果直接写result+=c; 那么result值为49。因为0对应的ASCII值从48开始
			}else{
				return (int) (isPositive?intResult:-intResult);
			}
		}
		return (int) (isPositive?intResult:-intResult);
	}

}
