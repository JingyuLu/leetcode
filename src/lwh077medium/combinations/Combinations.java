package lwh077medium.combinations;

import java.util.ArrayList;
import java.util.List;

public class Combinations {

	public static void main(String[] args) {

		Combinations c=new Combinations();
		
		int n=4;
		int k=2;
		
		List<List<Integer>> result=c.combine(n,k);
	}

	public List<List<Integer>> combine(int n, int k) {
		
		List<List<Integer>> res=new ArrayList<List<Integer>>();
		if(n<=0||k<=0) {
			return res;
		}
		
		List<Integer> list=new ArrayList<Integer>();
		combinationHelper(list,res,1,n,k);
		
		return res;
	}

	public void combinationHelper(List<Integer> list, List<List<Integer>> res, int index, int n, int k) {
		
		if(list.size()==k) {

			res.add(new ArrayList<Integer>(list));
			System.out.println("res: "+res);
			return;
		}
		for(int i=index;i<=n;i++) {
			list.add(i);
			combinationHelper(list, res, i+1, n, k);

			list.remove(list.size()-1);
			System.out.println("list: "+list);
		}
	}

}
