package lwh071medium.simplify_path;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;
 class SimplifyPath {

	public static void main(String[] args) {
		SimplifyPath sp=new SimplifyPath();
		
		String path="/a/./b/../../c/";
		
		String result=sp.simplifyPath(path);
	}

	private String simplifyPath(String path) {
		
		Stack<String> stack=new Stack<String>();

		Set<String> skip=new HashSet<String>(Arrays.asList("..",".",""));
		
		for(String dir:path.split("/")) {	

			if(dir.equals("..")&&!stack.isEmpty()) {
				stack.pop();
			}else if(!skip.contains(dir)) {
				stack.push(dir);
			}
		}
		String result="";
		if(stack.isEmpty()) {
			result+="/";
		}
		while(!stack.isEmpty()) {
			result="/"+stack.pop()+result;
		}
		return result;
	}

}
