package lwh037hard.sudoku_solver;

public class SudokuSolver {

	public static void main(String[] args) {
		
		SudokuSolver ss=new SudokuSolver();
		char[][] board={
				{'5','3','.','.','7','.','.','.','.'},
				{'6','.','.','1','9','5','.','.','.'},
				{'.','9','8','.','.','.','.','6','.'},
				{'8','.','.','.','6','.','.','.','3'},
				{'4','.','.','8','.','3','.','.','.'},
				{'7','.','.','.','2','.','.','.','6'},
				{'.','6','.','.','.','.','2','8','.'},
				{'.','.','.','4','1','9','.','.','5'},
				{'.','.','.','.','8','.','.','7','9'}
		};
		
		ss.solveSudoku(board);
	}

	public void solveSudoku(char[][] board) {
		
		if(board==null||board.length==0) {
			return;
		}
		solver(board);
	}

	public boolean solver(char[][] board) {
		
		for(int i=0;i<board.length;i++) {
			for(int j=0;j<board[0].length;j++) {
				if(board[i][j]=='.') {
					for(char c='1';c<='9';c++) {
						//put num to board; try from 1 to 9
						if(isValid(board,i,j,c)) {
							//add c to board cell
							board[i][j]=c;
							
							if(solver(board)) {
								//If it's the solution return true
								return true;
							}else {
								//rollback
								board[i][j]='.';
							}
						}
					}
					return false;
				}
			}
		}
		
		return true;
	}

	public boolean isValid(char[][] board, int row, int col, char c) {
		
		for(int i=0;i<9;i++) {
			//row
			if(board[i][col]!='.'&&board[i][col]==c)
				return false;
			//column
			if(board[row][i]!='.'&&board[row][i]==c)
				return false;
			//3*3
			if(board[3*(row/3)+i/3][3*(col/3)+i%3]!='.'&&board[3*(row/3)+i/3][3*(col/3)+i%3]==c)
				return false;
		}
		return true;
	}

}
