package lwh025hard.reverse_node;

import java.util.Stack;

public class ReverseKNode {

	public static void main(String[] args) {
		
		ReverseKNode rkn=new ReverseKNode();
		int k=2;
		ListNode head=new ListNode(1);
		
		ListNode result=rkn.reverseKGroup(head,k);
	}

	private ListNode reverseKGroup(ListNode head, int k) {
		
		//stack
		Stack<ListNode> st=new Stack<ListNode>();
		ListNode temp=head;
		for(int i=0;i<k;i++) {
			if(head!=null) {
				st.push(head);
			}else {
				return temp;
			}
			head=head.next;
		}
		
		ListNode reversedHead=st.pop();
		ListNode res=reversedHead;
		for(int i=0;i<k-1;i++) {
			reversedHead.next=st.pop();
			reversedHead=reversedHead.next;
		}
		reversedHead.next=reverseKGroup(head, k);
		
		return res;
	}

}
class ListNode {
	int val;
	ListNode next;

	ListNode(int x) {
		val = x;
	}
}