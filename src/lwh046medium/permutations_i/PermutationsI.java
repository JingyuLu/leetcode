package lwh046medium.permutations_i;

import java.util.ArrayList;
import java.util.List;

public class PermutationsI {

	// final result
	List<List<Integer>> rsList=new ArrayList<List<Integer>>();
	
	public static void main(String[] args) {
		PermutationsI p=new PermutationsI();
		
		int[] nums={1,2,3};
		
		List<List<Integer>> result=p.permutate(nums);
	}

	private List<List<Integer>> permutate(int[] nums) {
		int length=nums.length;
		if(nums==null||length==0){
			return rsList;
		}
		
		exchangeElements(nums,0,length);
		return rsList;
	}

	private void exchangeElements(int[] nums, int i, int length) {
		//dfs
		if(i==length){
			List<Integer> list=new ArrayList<>();
			for(int j=0;j<length;j++){
				list.add(nums[j]);
			}
			rsList.add(list);
		}
		
		for(int j=i;j<length;j++){
			swap(nums,i,j);
			exchangeElements(nums, i+1, length);
			swap(nums,i,j);
		}
	}

	private void swap(int[] nums, int i, int j) {
		int temp=nums[i];
		nums[i]=nums[j];
		nums[j]=temp;
	}

}
