package lwh046medium.permutations_i;

import java.util.ArrayList;
import java.util.List;

public class MyPermutations {
	
	public static void main(String[] args) {
		MyPermutations p=new MyPermutations();
		
		int[] nums={1,2,3};
		
		List<List<Integer>> result=p.permutate(nums);
	}


	public List<List<Integer>> permutate(int[] nums) {
		List<List<Integer>> rsList=new ArrayList<List<Integer>>();
		List<Integer> list=new ArrayList<>();
		int length=nums.length;
		if(nums==null||length==0){
			return rsList;
		}
		
		permutateHelper(rsList,list,nums,length,0);
		return rsList;

	}


	public void permutateHelper(List<List<Integer>> rsList, List<Integer> list, int[] nums, int len, int pos) {
		
		if(pos>len) {
			return;
		}else if(pos==len) {
			rsList.add(new ArrayList<>(list));
		}else {
			for(int i=0;i<len;i++) {
				if(!(list.contains(nums[i]))){
					list.add(nums[i]);
					permutateHelper(rsList,list,nums,len,pos+1);
					list.remove(list.size()-1);
				}
			}
		}
		
	}
}
