package lwh051hard.n_queens;

import java.util.ArrayList;
import java.util.List;

public class NQueens {

	public static void main(String[] args) {

		NQueens nq=new NQueens();
		int n=4;
		
		List<List<String>> result=nq.solveNQueens(n);
	}

	public List<List<String>> solveNQueens(int n) {
	
		List<List<String>> res=new ArrayList<List<String>>();
		int[] queenList=new int[n];
		placeQueen(res,queenList,n,0);	
		
		return res;
	}

	private void placeQueen(List<List<String>> res, int[] queenList, int n, int row) {
		
		if(row==n) {
			List<String> list=new ArrayList<String>();
			for(int i=0;i<n;i++) {
				String tmpStr="";
				for(int col=0;col<n;col++) {
					if(queenList[i]==col) {
						tmpStr+="Q";
					}else {
						tmpStr+=".";
					}
				}
				list.add(tmpStr);
			}
			res.add(list);
		}
		for(int col=0;col<n;col++) {
			if(isValid(queenList,row,col)) {	
				queenList[row]=col;
				placeQueen(res, queenList, n, row+1);
			}
		}
	}

	private boolean isValid(int[] queenList, int row, int col) {
		
		for(int i=0;i<row;i++) {
			int pos=queenList[i];
			if(pos==col) {	
				return false;
			}
			if(pos+row-i==col) {	
				return false;
			}
			if(pos-row+i==col) {	
				return false;
			}
		}
		
		return true;
	}

}
