package lwh094medium.binary_tree_inorder_traversal;

import java.util.ArrayList;
import java.util.List;

public class BTreeInorderTraversal {

	List<Integer> res=new ArrayList<>();
	public static void main(String[] args) {
		
		BTreeInorderTraversal bti=new BTreeInorderTraversal();
		TreeNode root=new TreeNode(1);
		root.right=new TreeNode(2);
		root.right.left=new TreeNode(3);
		
		bti.inorderTraversal(root);
		
		System.out.println(bti.res);
	}

	public List<Integer> inorderTraversal(TreeNode root) {

		if(root==null) {
			return res;
		}
		if(root.left!=null) {
			inorderTraversal(root.left);
		}
		res.add(root.val);
		if(root.right!=null) {
			inorderTraversal(root.right);
		}
		
		return res;
	}

}

class TreeNode{
	int val;
	TreeNode left;
	TreeNode right;
	public TreeNode(int x) {
		val=x;
	}
}