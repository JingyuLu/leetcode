package lwh080medium.remove_duplicates_from_sorted_array2;

public class RemoveDuplicates2 {

	public static void main(String[] args) {
		
		RemoveDuplicates2 rd2=new RemoveDuplicates2();
		
		int[] nums= {1,1,1,2,2,3};
		
		int result=rd2.removeDuplicates(nums);
	}

	public int removeDuplicates(int[] nums) {
		
		if(nums==null||nums.length==0) {
			return 0;
		}
		int index=0;
		int counter=0;
		
		for(int i=0;i<nums.length;i++) {
			if(i>0&&nums[i]==nums[i-1]) {	
				counter++;
				if(counter>2) {
					continue;	
				}
			}else {
				counter=1;
			}
			nums[index++]=nums[i];
		}
		return index;
		
	}

}
