package lwh115hard.distinct_subsequences;

public class DistinctSubsequences {

	public static void main(String[] args) {
		
		DistinctSubsequences ds=new DistinctSubsequences();
		String s = "rabbbit";
		String t= "rabbit";
		
		int result = ds.numDistinct(s,t);
		System.out.println(result);
	}

	public int numDistinct(String s, String t) {
		
		int[][] dp=new int[s.length()+1][t.length()+1];
		dp[0][0]=1;	//initial
		
		for(int j=1;j<=t.length();j++) {	//s is empty
			dp[0][j]=0;
		}
		for(int i=1;i<=s.length();i++) {	//t is empty
			dp[i][0]=1;
		}
		for(int i=1;i<=s.length();i++) {
			for(int j=1;j<=t.length();j++) {
				dp[i][j]=dp[i-1][j];
				if(s.charAt(i-1)==t.charAt(j-1)) {
					dp[i][j]+=dp[i-1][j-1];
				}
			}
		}
		
		return dp[s.length()][t.length()];
	}

}
