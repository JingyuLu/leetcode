package lwh014easy.longest_common_prefix;

public class MyLongestCommonPrefix {

	public static void main(String[] args) {

		MyLongestCommonPrefix mlsp = new MyLongestCommonPrefix();
		String[] strArray = {};

		String commonPrefix = mlsp.findLongestCommonPrefix(strArray);
	}

	private String findLongestCommonPrefix(String[] strArray) {
		
		if(strArray.length==0||strArray==null) {
			return "";
		}
		String prefix="";
		String temp="";
		int len=strArray[0].length();
		boolean mark=true;
		for(int i=0;i<strArray.length;i++) {
			if(strArray[i].length()<len) {
				len=strArray[i].length();
			}
		}
		for(int i=0;i<len;i++) {
			temp=strArray[0].substring(0, i+1);
			for(int j=0;j<strArray.length;j++) {
				if(!strArray[j].substring(0, i+1).equals(temp)) {
					mark=false;
				}
			}
			if(mark) {
				prefix=temp;
			}
		}
		
		return prefix;
	}

}
