package lwh014easy.longest_common_prefix;

/*
 *找一个字符串数组的最长公共前缀
 *
 *Write a function to find the longest common prefix string amongst an array of strings.
 */
public class LongestCommonPrefix {

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LongestCommonPrefix lcp=new LongestCommonPrefix();
		String[] strArray={};
		
		String commonPrefix=lcp.findLongestCommonPrefix(strArray);
	}

	/**
	 * @param strArray
	 * @return
	 */
	private String findLongestCommonPrefix(String[] strArray) {

		if(strArray.length==0||strArray==null){
			return "";
		}
		
		String prefix=strArray[0];
		
		for(int i=1;i<strArray.length;i++){
			while(strArray[i].indexOf(prefix)!=0){
				prefix=prefix.substring(0, prefix.length()-1);
				if(prefix.isEmpty()){
					return "";
				}
			}
		}
		
		return prefix;
	}

}
