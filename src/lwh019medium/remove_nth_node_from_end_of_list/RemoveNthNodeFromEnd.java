package lwh019medium.remove_nth_node_from_end_of_list;

import java.util.List;
import java.util.Scanner;

/*
Given a linked list, remove the nth node from the end of list and return its head.

For example,

   Given linked list: 1->2->3->4->5, and n = 2.

   After removing the second node from the end, the linked list becomes 1->2->3->5.
Note:
Given n will always be valid.
Try to do this in one pass.使用一次遍历解决问题
 */
public class RemoveNthNodeFromEnd {


	public static void main(String[] args) {
		RemoveNthNodeFromEnd removeNode=new RemoveNthNodeFromEnd();
		Scanner sc=new Scanner(System.in);
		ListNode head=new ListNode(1);
		
		int nodeNum=sc.nextInt();
		sc.nextLine();
		
		ListNode resultList=removeNode.removeNodeFromEnd(head,nodeNum);
	}

	/**
	 * @param head
	 * @param nodeNum
	 * @return
	 */
	private ListNode removeNodeFromEnd(ListNode head, int n) {
		
		if(head==null||head.next==null){
			return null;
		}
		
		ListNode fast=head;
		ListNode slow=head;
		//先让fast走出n步，使slow和fast之间差n步
		for(int i=0;i<n;i++){
			fast=fast.next;
		}
		//因为假设n为有效值所以不考虑空指针，即fast走完n/或者n-1步仍然不空
		if(fast==null){
			head=head.next;
			return head;
		}
		//找出fast为空时前一步的slow
		while(fast.next!=null){
			slow=slow.next;
			fast=fast.next;
		}
		
		slow.next=slow.next.next;	//如果一开始return head 的话此处将不会起作用

		return head;
	}

}
class ListNode {
   int val;
   ListNode next;
   ListNode(int x) { val = x; }
}
