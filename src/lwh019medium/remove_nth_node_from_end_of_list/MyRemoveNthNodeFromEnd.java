package lwh019medium.remove_nth_node_from_end_of_list;

import java.util.List;

import com.sun.jdi.Method;

public class MyRemoveNthNodeFromEnd {


	public static void main(String[] args) {
		MyRemoveNthNodeFromEnd mRemoveNode=new MyRemoveNthNodeFromEnd();
		MyListNode head=new MyListNode(1);
		int nodeNum=2;
		
		MyListNode resultList=mRemoveNode.removeNodeFromEnd(head,nodeNum);
	}

	private MyListNode removeNodeFromEnd(MyListNode head, int n) {
		
		if(head==null||head.next==null){
			return null;
		}
//		head.size() is not exist, cannot use size-n Method. So use fast slow pointer
		head.size();

		return head;
	}

}
class MyListNode {
   int val;
   MyListNode next;
   MyListNode(int x) { val = x; }
}
