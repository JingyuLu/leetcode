package lwh018medium.four_sum;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
Given an array S of n integers, are there elements a, b, c, and d in S such that a + b + c + d = target? Find all unique quadruplets in the array which gives the sum of target.

Note: The solution set must not contain duplicate quadruplets.

For example, given array S = [1, 0, -1, 0, -2, 2], and target = 0.

A solution set is:
[
  [-1,  0, 0, 1],
  [-2, -1, 1, 2],
  [-2,  0, 0, 2]
]
 */
public class FourSum1 {

	public static void main(String[] args) {
		FourSum1 fs=new FourSum1();
		
		int target=0;
		int[] inputArray={};
		
		List<List<Integer>> list=fs.fourSum(inputArray,target);
		
	}

	/**
	 * @param inputArray
	 * @param target
	 * @return
	 */
	public List<List<Integer>> fourSum(int[] nums, int target) {
		Arrays.sort(nums);
		ArrayList<List<Integer>> res = new ArrayList<List<Integer>>();
		for (int i = 0; i < nums.length - 3; i++) {
			if (i > 0 && nums[i] == nums[i - 1])
				continue;
			List<List<Integer>> curr = threeSum(nums, i, target - nums[i]);
			res.addAll(curr);
		}
		return res;
	}

	private List<List<Integer>> threeSum(int[] nums, int i, int target) {
		ArrayList<List<Integer>> res = new ArrayList<List<Integer>>();
		for (int j = i + 1; j < nums.length - 2; j++) {
			if (j > i + 1 && nums[j] == nums[j - 1])
				continue;
			List<List<Integer>> curr = twoSum(nums, i, j, target - nums[j]);
			res.addAll(curr);
		}
		return res;
	}

	private ArrayList<List<Integer>> twoSum(int[] nums, int i, int j, int target) {
		int left = j + 1, right = nums.length - 1;
		ArrayList<List<Integer>> res = new ArrayList<List<Integer>>();
		while (left < right) {
			if (nums[left] + nums[right] == target) {
				ArrayList<Integer> curr = new ArrayList<Integer>();
				curr.add(nums[i]);
				curr.add(nums[j]);
				curr.add(nums[left]);
				curr.add(nums[right]);
				res.add(curr);
				do {
					left++;
				} while (left < nums.length && nums[left] == nums[left - 1]);
				do {
					right--;
				} while (right >= 0 && nums[right] == nums[right + 1]);
			} else if (nums[left] + nums[right] > target) {
				right--;
			} else {
				left++;
			}
		}
		return res;
	}
}