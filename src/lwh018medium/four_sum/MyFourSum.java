package lwh018medium.four_sum;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
Given an array S of n integers, are there elements a, b, c, and d in S such that a + b + c + d = target? Find all unique quadruplets in the array which gives the sum of target.

Note: The solution set must not contain duplicate quadruplets.

For example, given array S = [1, 0, -1, 0, -2, 2], and target = 0.

A solution set is:
[
  [-1,  0, 0, 1],
  [-2, -1, 1, 2],
  [-2,  0, 0, 2]
]
 */
public class MyFourSum {

	public static void main(String[] args) {
		MyFourSum mfs=new MyFourSum();
		
		int target=0;
		int[] inputArray={};
		
		List<List<Integer>> list=mfs.fourSum(inputArray,target);
		
	}

	/**
	 * @param inputArray
	 * @param target
	 * @return
	 */
	private List<List<Integer>> fourSum(int[] nums, int target) {
		
		Arrays.sort(nums);
		ArrayList<List<Integer>> list4=new ArrayList<List<Integer>>();
		for(int i=0;i<nums.length;i++){
			if(i>=1&&nums[i]==nums[i-1]){
				continue;
			}else{
				int tempFour=target-nums[i];
				ArrayList<List<Integer>> curList4=threeSum(nums,tempFour,i,target);
				list4.addAll(curList4);
			}
		}
		return list4;
		
	}

	/**
	 * @param nums 
	 * @param tempFour
	 * @param i
	 * @param target 
	 * @return 
	 */
	private ArrayList<List<Integer>> threeSum(int[] nums, int tempFour, int i, int target) {
		ArrayList<List<Integer>> list3=new ArrayList<List<Integer>>();
		for(int j=i+1;j<nums.length;j++){
			if(nums[j]==nums[j-1]){
				continue;
			}else{
				int tempThree=tempFour-nums[j];
				ArrayList<List<Integer>> curList3=twoSum(nums,tempThree,i,j,target);
			}
		}
		return list3;
	}

	/**
	 * @param nums
	 * @param tempThree
	 * @param j
	 * @param target 
	 * @param j2 
	 * @return
	 */
	private ArrayList<List<Integer>> twoSum(int[] nums, int tempThree, int i, int j, int target) {
		int left=j+1;
		int right=nums.length-1;
		ArrayList<List<Integer>> list2=new ArrayList<List<Integer>>();
		while(left<right){
			if(nums[left]+nums[right]==tempThree){
				ArrayList<Integer> cur=new ArrayList<Integer>();
				cur.add(nums[i]);
				cur.add(nums[j]);
				cur.add(nums[left]);
				cur.add(nums[right]);
				list2.add(cur);
				do{
					left++;
				}while(left < nums.length && nums[left] == nums[left-1]);
//				while(left < right && nums[left] == nums[left-1]);
                do {
                    right--;
                } while(right >= 0 && nums[right] == nums[right+1]);
//                while(left<right && nums[right] == nums[right+1]);
            } else if (nums[left] + nums[right] > target){
                right--;
            } else {
                left++;
            }
		}
		
		return list2;
	}

}
