package lwh053easy.maximum_subarray;

public class MaximumSubArraySecond {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MaximumSubArraySecond ms=new MaximumSubArraySecond();
		
//		int[] targetArray={-2,1,-3,4,-1,2,1,-5,4};
		int[] targetArray={-2,-1};
//		int targetLength=targetArray.length;
		int maxSumCombo;
		
		maxSumCombo=ms.getMaxSubArraySum(targetArray);
		
		System.out.println("Max Sum: "+maxSumCombo);
	}

	private int getMaxSubArraySum(int[] nums) {
		// TODO Auto-generated method stub
		int i;
		int tempSum=0;
		int maxSum=getMinSum(nums);
		
		for(i=0;i<nums.length;i++){
			tempSum+=nums[i];
			
			if(tempSum<0&&tempSum>maxSum){
				maxSum=tempSum;
				tempSum=0;
			}else if(tempSum<0){
				tempSum=0;
			}else if(tempSum>=0&&tempSum>maxSum){
				maxSum=tempSum;
			}
		}
		return maxSum;
	}

	private int getMinSum(int[] targetArray) {
		// TODO Auto-generated method stub
		int minSum=0;
		for(int i=0;i<targetArray.length;i++){
			if(targetArray[i]<0){
				minSum+=targetArray[i];
			}
		}
		return minSum;
	}

}
