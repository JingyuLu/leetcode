package lwh053easy.maximum_subarray;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class MaximumSubarrayFirst {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MaximumSubarrayFirst ms=new MaximumSubarrayFirst();
		
		int[] targetArray={-2,1,-3,4,-1,2,1,-5,4};
		int targetLength=targetArray.length;
		int maxSumCombo;
		
		maxSumCombo=ms.getMaxSubArraySum(targetArray,targetLength);
		
		System.out.println("Max Sum: "+maxSumCombo);
	}

	private int getMaxSubArraySum(int[] targetArray, int targetLength) {

		int maxSum=getMinSum(targetArray);;
		int tempSum;
		int startIndex=0;
		int endIndex=0;
		int i,j,k;
		
		for(i=0;i<targetLength;i++){
			//start index of subarray
			for(j=i;j<targetLength;j++){
				//end index of subarray
				tempSum=0;
				for(k=i;k<=j;k++){
					//max sum calculation
					tempSum+=targetArray[k];
				}
				if(tempSum>maxSum){
					startIndex=i;
					endIndex=j;
					maxSum=tempSum;
					System.out.println(startIndex+" "+endIndex+" "+maxSum);
				}
			}
		}

		return maxSum;
	}

	private int getMinSum(int[] targetArray) {
		// TODO Auto-generated method stub
		int minSum=0;
		for(int i=0;i<targetArray.length;i++){
			if(targetArray[i]<0){
				minSum+=targetArray[i];
			}
		}
		return minSum;
	}

}
