package lwh026easy.remove_duplicates_from_sorted_array;

/*
 *
Given a sorted array, remove the duplicates in-place such that each element appear only once and return the new length.

Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.

Example:

Given nums = [1,1,2],

Your function should return length = 2, with the first two elements of nums being 1 and 2 respectively.
It doesn't matter what you leave beyond the new length.
 */
public class RemoveDuplicates {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		RemoveDuplicates rd=new RemoveDuplicates();
		
		int[] inputSortedArray={1,1,2};
		
		int temp=rd.removedDuplicates(inputSortedArray);
		
		System.out.println(temp);
	}

	private int removedDuplicates(int[] nums) {
		
		if(nums==null||nums.length==0){
			return 0;
		}
		
		//counter
		int counter=1;	//at least one element in array
		for(int i=0;i<nums.length-1;i++){
			for(int j=i+1;j<nums.length;j++){
				if(nums[i]!=nums[j]){
					nums[counter]=nums[j];
					counter++;
					i=j;
				}
			}
		}
		return counter;
	}

}
