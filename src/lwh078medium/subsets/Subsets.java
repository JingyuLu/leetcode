package lwh078medium.subsets;

import java.util.ArrayList;
import java.util.List;

public class Subsets {

	public static void main(String[] args) {
		
		Subsets ss=new Subsets();
		int[] nums= {1,2,3};
		
		List<List<Integer>> result=ss.subsets(nums);
	}

	public List<List<Integer>> subsets(int[] nums) {
		
		List<List<Integer>> res=new ArrayList<List<Integer>>();
		List<Integer> temp=new ArrayList<Integer>();
		dfs(res,temp,nums,0);
		
		return res;
	}

	public void dfs(List<List<Integer>> res, List<Integer> temp, int[] nums, int index) {
		
		res.add(new ArrayList<Integer>(temp));
		for(int i=index;i<=nums.length;i++) {
			temp.add(nums[i]);
			dfs(res,temp,nums,i+1);
			temp.remove(temp.size()-1);
		}
	}

}
