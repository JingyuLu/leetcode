package lwh069easy.sqrt;

import java.util.Scanner;

public class Sqrt {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Sqrt s=new Sqrt();
		Scanner sc=new Scanner(System.in);
		
		int inputNum=sc.nextInt();
		sc.nextLine();
		
		int result=s.sqrt(inputNum);
//		int r=(int) (3.8/1);   //=3
		System.out.println(result);
	}

	/**
	 * @param inputNum
	 * @return
	 */
	private int sqrt(int x) {
		
		if(x==0){
			return 0;
		}
		long left=1;
		long right=x/2+1;
		
		while(left<=right){
			long mid=(left+right)/2;
			long square=mid*mid;
			if(square==x){
				return (int) mid;
			}else if(square<x){
				left=mid+1;
			}else{
				right=mid-1;
			}
		}
		return (int) right;
		
		
	}

}
