package lwh112easy.path_sum;

public class PathSum {

	public static void main(String[] args) {

		PathSum ps=new PathSum();
		TreeNode root=new TreeNode(5);
		int sum=66;
		
		boolean result=ps.hasPathSum(root,sum);
	}

	public boolean hasPathSum(TreeNode root, int sum) {
		
		if(root==null) {
			return false;
		}else if(root.left==null&root.right==null) {
			return root.val==sum;
		}else {
			return hasPathSum(root.left, sum-root.val)||hasPathSum(root.right, sum-root.val);
		}
		
	}



}

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}