package lwh102medium.bst_level_order_traversal;

import java.util.ArrayList;
import java.util.List;

public class BSTLevelOrderTraversal {

	public static void main(String[] args) {
		BSTLevelOrderTraversal bst = new BSTLevelOrderTraversal();
		TreeNode root = new TreeNode(1);

		List<List<Integer>> result = bst.levelOrder(root);
	}

	public List<List<Integer>> levelOrder(TreeNode root) {
		List<List<Integer>> res = new ArrayList<>();
		
		levelOrderTraveral(root,res,0);
		
		return res;
	}

	public void levelOrderTraveral(TreeNode root, List<List<Integer>> res, int level) {
		
		if (root == null) {
			return;
		}
		if(res.size()<level+1) {	
			res.add(new ArrayList<Integer>());
		}
		res.get(level).add(root.val);
		levelOrderTraveral(root.left, res, level+1);
		levelOrderTraveral(root.right, res, level+1);
	}

}

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	public TreeNode(int x) {
		val = x;
	}
}