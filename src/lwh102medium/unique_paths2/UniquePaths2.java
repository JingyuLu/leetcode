package lwh102medium.unique_paths2;

public class UniquePaths2 {

	public static void main(String[] args) {
		
		UniquePaths2 up2=new UniquePaths2();
		
		int[][] obstacleGrid= {
				{0,0,0},
				{0,1,0},
				{0,0,0}
		};
		
		int result=up2.uniquePathsWithObstacles(obstacleGrid);
	}

	private int uniquePathsWithObstacles(int[][] obstacleGrid) {
		
		int m=obstacleGrid.length;
		int n=obstacleGrid[0].length;
		int[][] grid=new int[m][n];
		
		for(int i=0;i<m;i++) {
			if(obstacleGrid[i][0]!=1) {
				grid[i][0]=1;
			}else {
				break;	
			}
		}
		for(int j=0;j<n;j++) {
			if(obstacleGrid[0][j]!=1) {
				grid[0][j]=1;
			}else {
				break;
			}
		}
		
		for(int i=1;i<m;i++) {
			for(int j=1;j<n;j++) {
				if(obstacleGrid[i][j]==1) {
					grid[i][j]=0;
				}else {
					grid[i][j]=grid[i-1][j]+grid[i][j-1];
				}
			}
		}
		
		
		return grid[m-1][n-1];	
	}

}
