package lwh043medium.multiply_strings;

import java.util.ArrayList;
import java.util.List;

public class MultiplyString {


	public static void main(String[] args) {
		MultiplyString ms=new MultiplyString();
		
		String strNum1="666";
		String strNum2="666";
		
		String result=ms.multiplyStrings(strNum1,strNum2);
	}

	private String multiplyStrings(String num1, String num2) {
		
		int len1=num1.length();
		int len2=num2.length();
		int[] pos=new int[len1+len2];
		
		for(int i=len1-1;i>=0;i--) {
			for(int j=len2-1;j>=0;j--) {
				int tmp=(num1.charAt(i)-'0')*(num2.charAt(j)-'0');
				int p1=i+j;
				int p2=i+j+1;
				int sum=tmp+pos[p2];	
				
				pos[p1]+=sum/10;	//
				pos[p2]=sum%10;		//
			}
		}
		
		StringBuilder sb=new StringBuilder();
		for(int p:pos) {
			if(!(sb.length()==0&&p==0)) {
				sb.append(p);
			}
		}
		return sb.length()==0?"0":sb.toString();
	}

}
