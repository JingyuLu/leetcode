package lwh043medium.multiply_strings;

import java.util.ArrayList;
import java.util.List;

/*
Given two non-negative integers num1 and num2 represented as 
strings, return the product of num1 and num2.

Note:

The length of both num1 and num2 is < 110.
Both num1 and num2 contains only digits 0-9.
Both num1 and num2 does not contain any leading zero.
You must not use any built-in BigInteger library 
or convert the inputs to integer directly.
 */
public class MultiplyStrings {


	public static void main(String[] args) {
		MultiplyStrings ms=new MultiplyStrings();
		
		String strNum1="666";
		String strNum2="666";
		
		String result=ms.multiplyStrings(strNum1,strNum2);
	}

	private String multiplyStrings(String num1, String num2) {
		
		num1=new StringBuilder(num1).reverse().toString();
		num2=new StringBuilder(num2).reverse().toString();
		
		int[] d=new int[num1.length()+num2.length()];
		for(int i=0;i<num1.length();i++){
			int a=num1.charAt(i)-'0';
			for(int j=0;j<num2.length();j++){
				int b=num2.charAt(j)-'0';
				d[i+j]+=a*b;
			}
		}
		
		StringBuilder sb = new StringBuilder();
	    for (int i = 0; i < d.length; i++) {
	        int digit = d[i] % 10;
	        int carry = d[i] / 10;
	        sb.insert(0, digit);
	        if (i < d.length - 1)
	            d[i + 1] += carry;
	        }
	    //trim starting zeros
	    while (sb.length() > 0 && sb.charAt(0) == '0') {
	        sb.deleteCharAt(0);
	    }
	    return sb.length() == 0 ? "0" : sb.toString();
	}

}
