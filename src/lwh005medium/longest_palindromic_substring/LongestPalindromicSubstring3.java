package lwh005medium.longest_palindromic_substring;

/*
Given a string s, find the longest palindromic substring in s. 
You may assume that the maximum length of s is 1000
 */
public class LongestPalindromicSubstring3 {

	String longest="";	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		LongestPalindromicSubstring3 lps=new LongestPalindromicSubstring3();
		
		String inputStr="abcdefghg";
		
		String result=lps.findLongestPalindromicSubstring(inputStr);
		
		System.out.println(result);
	}

	/**
	 * @param inputStr
	 * @return
	 */
	private String findLongestPalindromicSubstring(String s) {

		//solution 中心扩散
		/*
动态规划虽然优化了时间，但也浪费了空间。实际上我们并不需要一直存储所有子字符串的回文情况，
我们需要知道的只是中心对称的较小一层是否是回文。所以如果我们从小到大连续以某点为个中心
的所有子字符串进行计算，就能省略这个空间。 这种解法中，外层循环遍历的是子字符串的中心点，
内层循环则是从中心扩散，一旦不是回文就不再计算其他以此为中心的较大的字符串。由于中心对称
有两种情况，一是奇数个字母以某个字母对称，而是偶数个字母以两个字母中间为对称，所以我们要
分别计算这两种对称情况。
		 */

		for (int i = 0; i < s.length(); i++) {
			// 计算奇数子字符串
			helper(s, i, 0);
			// 计算偶数子字符串
			helper(s, i, 1);
		}
		return longest;
	}

	private void helper(String s, int idx, int offset) {
		int left = idx;
		int right = idx + offset;
		while (left >= 0 && right < s.length() && s.charAt(left) == s.charAt(right)) {
			left--;
			right++;
		}
		// 截出当前最长的子串
		String currLongest = s.substring(left + 1, right);
		// 判断是否比全局最长还长
		if (currLongest.length() > longest.length()) {
			longest = currLongest;
		}
	}
}
