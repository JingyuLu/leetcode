package lwh005medium.longest_palindromic_substring;

/*
Given a string s, find the longest palindromic substring in s. 
You may assume that the maximum length of s is 1000
 */
public class MyLongestPalindromicSubstring {

	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		MyLongestPalindromicSubstring lps=new MyLongestPalindromicSubstring();
		
		String inputStr="abcdefghg";
		
		String result=lps.findLongestPalindromicSubstring(inputStr);
		
		System.out.println(result);
	}

	/**
	 * @param inputStr
	 * @return
	 */
	private String findLongestPalindromicSubstring(String s) {
		
		//solution 1 直接查找
		int maxLength=0;
		int maxStart=0;
		int len=s.length();
		
		for(int i=0;i<len;i++){	//i是子字符串长度
			//如果字符串长度为0，必定为回文
			for(int j=0;j<len-i;j++){	//j是子字符串起始位置
				if(isPalindromic(s,i,j)&&(i+1)>maxLength){
					maxLength=i+1;
					maxStart=j;
				}
			}	
		}
		return s.substring(maxStart, maxStart+maxLength);
	}

	/**
	 * @param s
	 * @param i
	 * @param j
	 * @return
	 */
	private boolean isPalindromic(String s, int i, int j) {
		//回文字符串对称
		int left=j;
		int right=j+i;
		while(left<right){
			if(s.charAt(left)!=s.charAt(right)){
				return false;
			}
			left++;
			right--;
		}
		return true;
	}

}
