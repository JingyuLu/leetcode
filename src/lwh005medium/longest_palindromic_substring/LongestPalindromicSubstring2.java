package lwh005medium.longest_palindromic_substring;

/*
Given a string s, find the longest palindromic substring in s. 
You may assume that the maximum length of s is 1000
 */
public class LongestPalindromicSubstring2 {

	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		LongestPalindromicSubstring2 lps=new LongestPalindromicSubstring2();
		
		String inputStr="abcdefghg";
		
		String result=lps.findLongestPalindromicSubstring(inputStr);
		
		System.out.println(result);
	}

	/**
	 * @param inputStr
	 * @return
	 */
	private String findLongestPalindromicSubstring(String s) {
		
		//solution 2 动态规划 wrong
		/*
	思路
	根据回文的特性，一个大回文按比例缩小后的字符串也必定是回文，比如ABCCBA，那BCCB肯定也是回文。
	所以我们可以根据动态规划的两个特点：第一大问题拆解为小问题，第二重复利用之前的计算结果，来解答这道题。
	那如何划分小问题呢，我们可以先把所有长度最短为1的子字符串计算出来，根据起始位置从左向右，这些必定是回文。
	然后计算所有长度为2的子字符串，再根据起始位置从左向右。到长度为3的时候，我们就可以利用上次的计算结果：
	如果中心对称的短字符串不是回文，那长字符串也不是，如果短字符串是回文，那就要看长字符串两头是否一样。
	这样，一直到长度最大的子字符串，我们就把整个字符串集穷举完了，但是由于使用动态规划，
	使计算时间从O(N^3)减少到O(n^2)。

	注意
	外循环的变量控制的实际上不是字符串长度，而是字符串首到尾的增量

	二维数组的第一维是指子字符串起始位置，第二维是指终止位置，所存数据表示是否回文
		 */
//		if(s==null){
//			return "";
//		}else if(s.length()==1){
//			return s;
//		}
		int maxLength = 0;
        int maxStart = 0;
        int len = s.length();
        
        boolean[][] dp=new boolean[len][len];
		
        for(int i=0;i<len;i++){	//i是子字符串长度
        	for(int j=0;j<len-i;j++){	//j是字符串起始位置
        		if(i==0||i==1){
        			//长度为0或者1必然是回文
        			dp[j][j+i]=true;
        		}else if(s.charAt(j)==s.charAt(j+i)){
        			//如果左右两端相等，那只要中心对称子字符串是回文就是回文
        			dp[j][j+i]=dp[j+1][j+i-1];
        		}else{
        			dp[j][j+i]=false;
        		}
        		
        		if(dp[j][j+i]&&i>maxLength){
        			maxLength=i+1;
        			maxStart=j;
        		}
        	}
        }
		return s.substring(maxStart,maxStart + maxLength);
	}
}
