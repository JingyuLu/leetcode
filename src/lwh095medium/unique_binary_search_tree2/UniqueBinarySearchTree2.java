package lwh095medium.unique_binary_search_tree2;

import java.util.ArrayList;
import java.util.List;

public class UniqueBinarySearchTree2 {

	public static void main(String[] args) {

		UniqueBinarySearchTree2 ubst2 = new UniqueBinarySearchTree2();
		int n = 6;

		List<TreeNode> result = ubst2.generateTrees(n);
	}

	public List<TreeNode> generateTrees(int n) {
		
		if(n<=0) {
			return new ArrayList<TreeNode>();
		}
		return generateSubTrees(1,n);
		
	}

	public List<TreeNode> generateSubTrees(int start, int end) {
		
		List<TreeNode> result=new ArrayList<TreeNode>();
		if(start>end) {
			result.add(null);
			return result;
		}
		for(int i=start;i<=end;i++) {
			for(TreeNode leftSubTreeRoot:generateSubTrees(start, i-1)) {
				for(TreeNode rightSubTreeRoot:generateSubTrees(i+1, end)) {
					TreeNode root=new TreeNode(i);
					root.left=leftSubTreeRoot;
					root.right=rightSubTreeRoot;
					result.add(root);
				}
			}
		}
		
		return result;
	}

}

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	public TreeNode(int x) {
		val = x;
	}
}