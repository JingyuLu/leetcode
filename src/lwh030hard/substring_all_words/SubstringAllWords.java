package lwh030hard.substring_all_words;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubstringAllWords {

	public static void main(String[] args) {

		SubstringAllWords saw = new SubstringAllWords();
		String s = "barfoothefoobarman";
		String[] words = { "foo", "bar" };

		List<Integer> res = saw.findSubstring(s, words);
	}

	private List<Integer> findSubstring(String s, String[] words) {

		List<Integer> list = new ArrayList<Integer>();
		Map<String, Integer> map = new HashMap<String, Integer>();
		Map<String, Integer> tmp = new HashMap<String, Integer>();
		int sLength = s.length();
		int wordNum = words.length;
		int j = 0;

		if(wordNum == 0) {
			return list;
		}
		int wordsLength = words[0].length();
		if (sLength < wordsLength) {
			return list;
		}
		for (int i = 0; i < wordNum; i++) {
			if (map.containsKey(words[i])) {
				map.put(words[i], map.get(words[i]) + 1);
			} else {
				map.put(words[i], 1);
			}
		}

		for (int i = 0; i <= sLength - wordNum * wordsLength; i++) {
			tmp.clear();
			for (j = 0; j < wordNum; j++) {
				String word = s.substring(i + j * wordsLength, i + j * wordsLength + wordsLength);
				if (!map.containsKey(word))
					break;
				if (tmp.containsKey(word))
					tmp.put(word, tmp.get(word) + 1);
				else
					tmp.put(word, 1);
				if (tmp.get(word) > map.get(word))
					break;
			}
			if (j == wordNum) {
				list.add(i);
			}
		}

		return list;
	}

}
