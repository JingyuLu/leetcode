package lwh114medium.flatten_binary_tree_to_linked_list;

public class FlattenBT {

	public static void main(String[] args) {

		FlattenBT fbt = new FlattenBT();
		TreeNode  root=new TreeNode(1);
		
		fbt.flatten(root);
	}

	public void flatten(TreeNode root) {
		
		if(root==null) {
			return;
		}
		if(root.left!=null) {
			TreeNode cur=root.left;
			while(cur.right!=null) {
				cur=cur.right;
			}
			cur.right=root.right;
			root.right=root.left;
			root.left=null;
		}
		flatten(root.right);
		
	}

}

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}