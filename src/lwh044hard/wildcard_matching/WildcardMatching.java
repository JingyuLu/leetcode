package lwh044hard.wildcard_matching;

public class WildcardMatching {

	public static void main(String[] args) {

		WildcardMatching wm=new WildcardMatching();
		String s = "aa";
		String p = "a";
		
		boolean result=wm.isMatch(s,p);
	}

	private boolean isMatch(String s, String p) {
	
		//double pointer
		int sp=0;
		int pp=0;
		int match=0;
		int startIdx=-1;
		
		while(sp<s.length()) {
			if(pp<p.length()&&(p.charAt(pp)=='?'||s.charAt(sp)==p.charAt(pp))){
				sp++;
				pp++;
			}else if(pp<p.length()&&p.charAt(pp)=='*') {
				startIdx=pp;
				match=sp;
				pp++;
			}else if(startIdx!=-1) {
				pp=startIdx+1;
				match++;
				sp=match;
			}else {
				return false;
			}
		}
		
		while(pp<p.length()&&p.charAt(pp)=='*') {
			pp++;
		}
		
		return pp==p.length();
	}

}
