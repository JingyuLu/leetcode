package lwh067easy.add_binary;

import lwh007easy.reverse_integer.ReverseInteger;

public class AddBinary {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
			
		AddBinary ab=new AddBinary();
		
		String a="11";
		String b="1";
		
		String result=ab.addBinary(a,b);
		System.out.println(result);
	}

	public String addBinary(String a, String b) {
		
		char[] aAry=a.toCharArray();
		char[] bAry=b.toCharArray();
		StringBuilder sb=new StringBuilder();
		
		int i=aAry.length-1;
		int j=bAry.length-1;
		int aByte;
		int bByte;
		int carry=0;
		int result;
		
		while(i>=0||j>=0||carry==1) {
			aByte=(i>=0)?(aAry[i--]-'0'):0;
			bByte=(j>=0)?(bAry[j--]-'0'):0;
			result=aByte^bByte^carry;	
			carry=((aByte+bByte+carry)>=2)?1:0;
			sb.append(result);
		}
		return sb.reverse().toString();
	}

}
