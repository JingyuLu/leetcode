package lwh038easy.count_and_say;

import java.util.Scanner;

public class CountAndSay {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CountAndSay cas=new CountAndSay();
		
		Scanner sc=new Scanner(System.in);
		
		int n=sc.nextInt();
		sc.nextLine();
		
		cas.countAndSay(n);
	}


	private String countAndSay(int n) {
		
		if(n==1){
			return "1";
		}
		
		if (n <= 0) {  
		    return null;  
		}  
		String s = "1";  
		int num = 1;  
		for (int j = 0; j < n - 1; j++) {  
		    StringBuilder sb = new StringBuilder();  
		    for (int i = 0; i < s.length(); i++) {  
		        if (i < s.length() - 1 && s.charAt(i) == s.charAt(i + 1)) {  
		            num++;  
		        } else {  
		            sb.append(num + "" + s.charAt(i));  
		            num = 1;  
		        }  
		    }  
		    s = sb.toString();  
		}  
		return s;  
	}

}


/*
1
11
21
1211
111221
312211
13112221
1113213211
31131211131221
13211311123113112211
11131221133112132113212221
3113112221232112111312211312113211
1321132132111213122112311311222113111221131221
11131221131211131231121113112221121321132132211331222113112211
311311222113111231131112132112311321322112111312211312111322212311322113212221
*/
