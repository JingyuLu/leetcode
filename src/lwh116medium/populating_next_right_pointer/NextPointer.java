package lwh116medium.populating_next_right_pointer;

public class NextPointer {

	public static void main(String[] args) {
		NextPointer np = new NextPointer();
		TreeLinkNode root = new TreeLinkNode(1);

		np.connect(root);
	}

	public void connect(TreeLinkNode root) {

		if (root == null) {
			return;
		}
		if (root.left != null) {
			root.left.next = root.right;
		}
		if (root.right != null) {
			root.right.next=(root.next ==null?root.next.right:root.next.left);
		}
		connect(root.left);
		connect(root.right);
	}

}

class TreeLinkNode {
	int val;
	TreeLinkNode left, right, next;

	TreeLinkNode(int x) {
		val = x;
	}
}