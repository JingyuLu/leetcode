package lwh103medium.bst_zigzag_level_order_traversal;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class BSTZigzagLevelOrderTraversal {

	public static void main(String[] args) {
		BSTZigzagLevelOrderTraversal zz=new BSTZigzagLevelOrderTraversal();
		TreeNode root=new TreeNode(1);
		
		List<List<Integer>> result=zz.zigzagLevelOrder(root);
	}

	public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
		
		List<List<Integer>> res=new ArrayList<List<Integer>>();
		
		zigzagLevelOrderTraversal(root,res,0);
		
		return res;
	}

	public void zigzagLevelOrderTraversal(TreeNode root, List<List<Integer>> res, int level) {
		if(root==null) {
			return;
		}
		if(res.size()<level+1) {
			res.add(new LinkedList<Integer>());
		}
		
		if(level%2!=0) {
			((LinkedList<Integer>) res.get(level)).addFirst(root.val);
		}else {
			res.get(level).add(root.val);
		}
		zigzagLevelOrderTraversal(root.left, res, level+1);
		zigzagLevelOrderTraversal(root.right, res, level+1);
	}

}
class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	public TreeNode(int x) {
		val = x;
	}
}