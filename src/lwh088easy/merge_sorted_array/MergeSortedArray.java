package lwh088easy.merge_sorted_array;

import java.util.Scanner;

public class MergeSortedArray {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		MergeSortedArray msa=new MergeSortedArray();
		
		int m=1;
		int n=0;
		
		int[] nums1={1};
		int[] nums2={};
		
		msa.mergeSortedArray(nums1,m,nums2,n);
		
		for(int i:nums1){
			System.out.print(i+" ");
		}
	}

	/**
	 * @param nums1
	 * @param m
	 * @param nums2
	 * @param n
	 */
	private void mergeSortedArray(int[] nums1, int m, int[] nums2, int n) {
		int maxIndex=m+n-1;
		m=m-1;
		n=n-1;
		
//		m=m-1;
//		n=n-1;
//		int maxIndex=m+n+1;
		
		while(m>=0||n>=0){
			if(m<0){
				nums1[maxIndex]=nums2[n];
				maxIndex--;
				n--;
			}else if(n<0){
				nums1[maxIndex]=nums1[m];
				maxIndex--;
				m--;
			}else{
				nums1[maxIndex]=nums1[m]>nums2[n]?nums1[m--]:nums2[n--];	
				maxIndex--;
			}
		}
	}

}
