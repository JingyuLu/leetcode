package lwh090medium.subsets2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Subsets2 {

	public static void main(String[] args) {
		
		Subsets2 ss2=new Subsets2();
		int[] nums= {1,2,2};
		
		List<List<Integer>> result=ss2.subsetsWithDup(nums);
	}

	public List<List<Integer>> subsetsWithDup(int[] nums) {
		
		List<List<Integer>> res=new ArrayList<List<Integer>>();
		List<Integer> list=new ArrayList<Integer>();
		Arrays.sort(nums);
		
		subsetsHelper(res,list,nums,0);
		
		return res;
	}

	public void subsetsHelper(List<List<Integer>> res, List<Integer> list, int[] nums, int index) {
		
		res.add(new ArrayList<Integer>(list));	
		
		for(int i=index;i<nums.length;i++) {
			if(i==index||nums[i]!=nums[i-1]) {	
				list.add(nums[i]);
				subsetsHelper(res, list, nums, i+1);
				list.remove(list.size()-1);
			}
		}
	}

}
