package lwh024medium.swap_nodes_in_pairs;

/*
Given a linked list, swap every two adjacent nodes and return its head.

For example,
Given 1->2->3->4, you should return the list as 2->1->4->3.

Your algorithm should use only constant space. You may not modify 
the values in the list, only nodes itself can be changed.
 */
public class SwapNodesInPairs {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SwapNodesInPairs snip=new SwapNodesInPairs();
		
		ListNode head=new ListNode(0);
		ListNode result=snip.swapNodesInPairs(head);
	}

	/**
	 * @param head 
	 * @return
	 */
	private ListNode swapNodesInPairs(ListNode head) {
		
		if(head==null||head.next==null){
			return head;
		}
		ListNode temp=head.next;
		head.next=swapNodesInPairs(temp.next);
		temp.next=head;
		
		return temp;
	}

}

class ListNode {
  int val;
  ListNode next;
  ListNode(int x) { val = x; } 
}