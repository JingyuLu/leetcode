package lwh098medium.validate_binary_search_tree;


public class ValidateBinarySearchTree {

	public static void main(String[] args) {
		
		ValidateBinarySearchTree vbst=new ValidateBinarySearchTree();
		TreeNode root=new TreeNode(5);
		
		boolean result=vbst.isValidBST(root);
	}

	public boolean isValidBST(TreeNode root) {
		
		if(root==null||root.left==null&&root.right==null) {
			return true;
		}
		return validBST(root,Long.MIN_VALUE,Long.MAX_VALUE);
	}

	public boolean validBST(TreeNode root, long minValue, long maxValue) {
		
		if(root==null) {
			return true;
		}
		if(root.val<=minValue||root.val>=maxValue) {
			return false;
		}
		
		return validBST(root.left, minValue, root.val)&&validBST(root.right, root.val, maxValue);
	}

}

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	public TreeNode(int x) {
		val = x;
	}
}