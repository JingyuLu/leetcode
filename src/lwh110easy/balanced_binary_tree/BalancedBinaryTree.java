package lwh110easy.balanced_binary_tree;

public class BalancedBinaryTree {

	public static void main(String[] args) {

		BalancedBinaryTree bbt = new BalancedBinaryTree();
		TreeNode root=new TreeNode(3);
		
		boolean result=bbt.isBalanced(root);
	}

	public boolean isBalanced(TreeNode root) {
		
		if(root==null) {
			return true;
		}
	
		int leftHeight=treeHeight(root.left);
		int rightHeight=treeHeight(root.right);
		
		if(Math.abs(leftHeight-rightHeight)<=1) {
			return isBalanced(root.left)&&isBalanced(root.right);
		}else {
			return false;
		}
	}

	public int treeHeight(TreeNode root) {
		
		if(root==null) {
			return 0;
		}else if(root.left==null&&root.right==null) {
			return 1;
		}else {
			int left=treeHeight(root.left);
			int right=treeHeight(root.right);
			return (left>right?left:right)+1;
		}
	}

}

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}