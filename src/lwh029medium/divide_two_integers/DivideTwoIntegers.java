package lwh029medium.divide_two_integers;


public class DivideTwoIntegers {

	public static void main(String[] args) {

		DivideTwoIntegers dti=new DivideTwoIntegers();
		
		int dividend=-2147483648;
		int divisor=-1;
		
		int result=dti.divide(dividend,divisor);
		
		System.out.println(result);

	}

	private int divide(int dividend, int divisor) {
		int res=0;
		int tempMark=(dividend^divisor)>>31;
		
		long tempDividend=0;
		if(dividend<Integer.MIN_VALUE) {
			tempDividend=(long)2147483647+1;
		}else {
			tempDividend=Math.abs(dividend);
		}
		long tempDivisor=Math.abs(divisor);
		System.out.println(tempDividend+" "+tempDivisor);
		
		if(tempDivisor==1) {
			System.out.println(tempMark+" "+tempDividend);
			return tempMark<0?(int)(-tempDividend):(int)(tempDividend);
		}
		
		while(tempDividend>=tempDivisor) {
			long tempNum=tempDivisor;
			for(int i=0;tempNum<=tempDividend;i++) {
				tempDividend-=tempNum;

				res+=1<<i;
				tempNum=tempNum<<1;
			}
		}
		return tempMark<0?(int)(-res):(int)(res);
	}

}
