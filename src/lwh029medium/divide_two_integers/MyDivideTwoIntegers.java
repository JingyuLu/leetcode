package lwh029medium.divide_two_integers;

public class MyDivideTwoIntegers {

	public static void main(String[] args) {

		MyDivideTwoIntegers dti=new MyDivideTwoIntegers();
		
		int dividend=-2147483648;
		int divisor=-1;
		
		int result=dti.divide(dividend,divisor);
		
		System.out.println(result);

	}

	public int divide(int dividend, int divisor) {
		
		boolean isPos=true;
		long res=0;
		if((dividend<0&&divisor>0)||(dividend>0&&divisor<0)) {
			isPos=false;
		}
		
		long ld=Math.abs((long)dividend);
		long lr=Math.abs((long)divisor);
		
		//The divisor will never be 0
		if(ld==0||ld<lr) {
			return 0;
		}
		
		res=divide(ld,lr);
		if(res>Integer.MAX_VALUE) {
			return isPos?Integer.MAX_VALUE:Integer.MIN_VALUE;
		}
		
		return (int) (isPos?res:-res);		
	}

	public long divide(long ld, long lr) {

		if(ld<lr) {
			return 0;
		}
		//half divide
		long result=1;
		long sum=lr;
		
		while(ld>=(sum+sum)) {
			sum+=sum;
			result+=result;
		}
		
		return result+divide(ld-sum,lr);
	}

}
