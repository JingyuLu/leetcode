package lwh004hard.median_of_two_sorted_array;

/*
 * median 中位数
 */
public class MedianOfArrays {

	public static void main(String[] args) {

		MedianOfArrays moa = new MedianOfArrays();
		int[] nums1 = { 1, 2 };
		int[] nums2 = { 3, 4 };
//		System.out.println((5+6)/2);
		double result = moa.findMedianSortedArrays(nums1, nums2);
	}

	public double findMedianSortedArrays(int[] nums1, int[] nums2) {

		int l1 = nums1.length;
		int l2 = nums2.length;
		int mid = (l1 + l2) / 2;
		int[] s = new int[mid + 1];	//the mid+1 number is median
		int j = 0;
		int k = 0;

		for (int i = 0; i < mid + 1; i++) {
			if (j < l1 && (k >= l2 || nums1[j] < nums2[k])) {
				s[i] = nums1[j++];
			} else {
				s[i] = nums2[k++];
			}
		}
		/*
		 * 返回值务必加上cast，否则返回值先四舍五入得到int在转换为double 如本例子，不加cast返回2.0，添加cast返回2.5
		 */
		if ((l1 + l2) % 2 != 0) {
			// 奇
			return (double) s[mid];	//array starts from 0,mid is the mid+1 digit number in array
		} else {
			// 偶
			return (double) (s[mid - 1] + s[mid]) / 2;
		}

	}
}
