package lwh040medium.combination_sum2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lwh039medium.combination_sum.CombinationSum;

public class CombinationSum2 {

	List<List<Integer>> list;
	
	public static void main(String[] args) {
		CombinationSum2 cs=new CombinationSum2();
		
		int[] nums={2,3,6,7}; 
		int target=7;
		
		List<List<Integer>> result=cs.combinationSum2(nums, target);
	}

	public List<List<Integer>> combinationSum2(int[] candidates, int target) {
		list=new ArrayList<List<Integer>>();
		List<Integer> temp=new ArrayList<Integer>();
		Arrays.sort(candidates);
		helper(temp,candidates,target,0);
		return list;
	}

	public void helper(List<Integer> temp, int[] nums, int target, int index) {
		if(target<0){
			return;
		}else if(target==0){
			List<Integer> oneCombo=new ArrayList<Integer>(temp);
			list.add(oneCombo);
		}else{
			for(int i=index;i<nums.length;i++){
				temp.add(nums[i]);

				helper(temp,nums,target-nums[i],i+1);
				temp.remove(temp.size()-1);
				while(i<nums.length-1&&nums[i]==nums[i+1]){
					i++;
				}
			}
		}
		
	}

}
