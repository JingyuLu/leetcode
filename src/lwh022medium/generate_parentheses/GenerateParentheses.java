package lwh022medium.generate_parentheses;

import java.util.ArrayList;
import java.util.List;

/*
Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses.

For example, given n = 3, a solution set is:

[
  "((()))",
  "(()())",
  "(())()",
  "()(())",
  "()()()"
]
 */
public class GenerateParentheses {	//��ȡС����

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		GenerateParentheses gp=new GenerateParentheses();
		 int pairNum=3;
		 
		 List<String> result=gp.generateParentheses(pairNum);
//		 for(String s:result){
//			 System.out.println(s);
//		 }
	}

	/**
	 * @param pairNum
	 * @return
	 */
	private List<String> generateParentheses(int n) {

		List<String> res=new ArrayList<String>();
		String temp="";
		generateParenthesesHelper(res,temp,n,n);
		return res;
	}

	/**
	 * @param res
	 * @param temp
	 * @param n
	 * @param n2
	 */
	private void generateParenthesesHelper(List<String> res, String temp, int left, int right) {
		
		if(left==0&&right==0){
			res.add(temp);
//			for(String s:res){
//				 System.out.println(s);
//			}
			return;
		}
		if(left>0){
			generateParenthesesHelper(res, temp+"(", left-1, right);
		}
		if(right>0&&left<right){
			generateParenthesesHelper(res, temp+")", left, right-1);
		}
	}

}
