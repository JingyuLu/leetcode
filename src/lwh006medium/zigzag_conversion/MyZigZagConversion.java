package lwh006medium.zigzag_conversion;

public class MyZigZagConversion {
	
	boolean direction=true;
	int rows=0;
	int cols=0;
	
	public static void main(String[] args) {
		MyZigZagConversion mzzc=new MyZigZagConversion();
		
		String inputStr="PAYPALISHIRING";
		String result=mzzc.convert(inputStr,3);
		
		System.out.println(result);
	}

	public String convert(String s, int numRows) {
		
		if(numRows==1) {
			return s;
		}
		int strLen=s.length();
		int strPointer=0;
		String res="";
		String[][] str=new String[numRows][strLen];
		
		for(int i=0;i<numRows;i++) {
			for(int j=0;j<strLen;j++) {
				str[i][j]=" ";
			}
		}

		//put all letter in array
		while(strPointer<strLen) {
			if(direction) {
				str[rows][cols]=s.substring(strPointer, strPointer+1);
				rows++;
				if(rows==numRows) {
					direction=false;
					rows--;
					cols++;
				}
				strPointer++;
			}else {
				rows--;
				str[rows][cols]=str[rows][cols]=s.substring(strPointer, strPointer+1);
				cols++;
				if(rows==0) {
					direction=true;
					rows++;
					cols--;
				}
				strPointer++;
			}
		}
		
//		//print the result of 2 dimension array
//		for(int i=0;i<numRows;i++) {
//			for(int j=0;j<strLen;j++) {
//				System.out.print(str[i][j]);
//			}
//			System.out.println();
//		}
		
		res=arrayToString(str,numRows,strLen);
		
		return res;
	}

	public String arrayToString(String[][] str, int numRows, int strLen) {
		
		String result="";
		for(int i=0;i<numRows;i++) {
			for(int j=0;j<strLen;j++) {
				if(str[i][j]!=" ") {
					result+=str[i][j];
				}
			}
		}
		return result;
	}
}
