package lwh045hard.jump_game2;

public class MyJumpGame2 {

	public static void main(String[] args) {
		MyJumpGame2 jg2 = new MyJumpGame2();

		int[] nums = { 3, 2, 1, 0, 4 }; // [3,2,1,2,4]true???

		int result = jg2.jump(nums);
		System.out.println(result);
	}

	private int jump(int[] nums) {

		int lastStep=0;	
		int curStep=0;	
		int step=0;
		int len=nums.length;
		
		for(int i=0;i<len;i++) {
			if(lastStep<i) {	 
				step++;
				lastStep=curStep;	 
			}
			int tmp=nums[i]+i; 
			if(curStep<tmp) {
				curStep=tmp;	
			}
		}
		return curStep>=len-1?step:0;	 
	}

}
