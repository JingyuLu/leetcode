package lwh045hard.jump_game2;

public class JumpGame2 {

	public static void main(String[] args) {
		JumpGame2 jg2 = new JumpGame2();

		int[] nums = { 3, 2, 1, 0, 4 }; // [3,2,1,2,4]true???只判断是否会达到最后一个index，是否超出不管

		int result = jg2.jump(nums);
		System.out.println(result);
	}

	/*
	 * each element is the maximum jump length, means jump<=element
	 */
	private int jump(int[] nums) {
		
		int length=nums.length;
		int reach=nums[0];
		int lastReach=0;
		int step=0;
		
		if(length<=1) {
			return 0;
		}
		
		for(int i=1;i<=reach&&i<length;i++) {
			if(i>lastReach) {
				step++;
				lastReach=reach;
			}
			reach=Math.max(reach, i+nums[i]);
		}
		if(reach<length-1) {
			return 0;
		}
		
		return step;
	}

}
