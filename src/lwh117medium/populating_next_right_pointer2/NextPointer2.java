package lwh117medium.populating_next_right_pointer2;

public class NextPointer2 {

	public static void main(String[] args) {
		NextPointer2 np2=new NextPointer2();
		TreeLinkNode root=new TreeLinkNode(1);
		
		np2.connect(root);
	}

	public void connect(TreeLinkNode root) {
		TreeLinkNode head=root;
		while(head!=null) {
			TreeLinkNode cur=new TreeLinkNode(0);
			TreeLinkNode tmp=cur;
			while(head!=null) {
				if(head.left!=null) {
					cur.next=head.left;
					cur=cur.next;
				}
				if(head.right!=null) {
					cur.next=head.right;
					cur=cur.next;
				}
				head=head.next;
			}
			head=tmp.next;
		}
		
	}

}
class TreeLinkNode {
	int val;
	TreeLinkNode left, right, next;

	TreeLinkNode(int x) {
		val = x;
	}
}