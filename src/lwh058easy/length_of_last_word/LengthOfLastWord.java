package lwh058easy.length_of_last_word;

public class LengthOfLastWord {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		LengthOfLastWord lol=new LengthOfLastWord();
		String inputString="Hello World";
		
		int length=lol.lengthOfLastWord(inputString);
		System.out.println(length);

	}

	private int lengthOfLastWord(String s) {
		
		String[] strArray=s.split(" ");

		if(strArray==null||strArray.length==0){
			return 0;
		}else{
			return strArray[strArray.length-1].length();
		}
	}

}
