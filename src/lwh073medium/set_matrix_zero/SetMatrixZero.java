package lwh073medium.set_matrix_zero;

public class SetMatrixZero {

	public static void main(String[] args) {

		SetMatrixZero smz = new SetMatrixZero();

		int[][] matrix = { { 1, 1, 1 }, { 1, 0, 1 }, { 1, 1, 1 } };

		smz.setZeroes(matrix);
	}

	private void setZeroes(int[][] matrix) {
		if (matrix.length == 0) {
			return;
		}

		boolean firstRowZero = false;
		boolean firstColZero = false;

		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				if (i != 0 && j != 0 && matrix[i][j] == 0) {
					matrix[i][0] = 0;
					matrix[0][j] = 0;
				} else if (matrix[i][j] == 0) {

					firstRowZero = (i == 0 ? true : firstRowZero);
					firstColZero = (j == 0 ? true : firstColZero);
				}
			}
		}

		for (int i = 1; i < matrix.length; i++) {
			for (int j = 1; j < matrix[0].length; j++) {
				if (matrix[i][0] == 0 || matrix[0][j] == 0) {
					matrix[i][j] = 0;
				}
			}
		}

		for (int i = 0; firstColZero && i < matrix.length; i++) {
			matrix[i][0] = 0;
		}

		for (int j = 0; firstRowZero && j < matrix[0].length; j++) {
			matrix[0][j] = 0;
		}
	}

}
