package lwh012medium.integer_to_roman;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

/*
Given an integer, convert it to a roman numeral.

Input is guaranteed to be within the range from 1 to 3999.
 */
public class IntegerToRoman {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		IntegerToRoman itr=new IntegerToRoman();
		Scanner sc=new Scanner(System.in);
		
		int inputNum=sc.nextInt();
		sc.nextLine();
		
		String romanStr=itr.integerToRoman(inputNum);
				
	}

	/**
	 * @param inputNum
	 * @return
	 */
	private String integerToRoman(int num) {
		//solution 1
		String[] symbol={"M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"};
		int[] value={1000,900,500,400,100,90,50,40,10,9,5,4,1};
		StringBuilder roman=new StringBuilder();
		
		int i=0;
		while(num>0){
			if(num>=value[i]){	//如num>value[i]数组越界
				roman.append(symbol[i]);
				num=num-value[i];
			}else{
				i++;
			}
		}
		return roman.toString();

	}

}
