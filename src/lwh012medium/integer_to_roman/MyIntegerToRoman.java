package lwh012medium.integer_to_roman;

import java.util.Scanner;

public class MyIntegerToRoman {

	public static void main(String[] args) {

		MyIntegerToRoman mitr = new MyIntegerToRoman();
		int num = 3;

		String romanStr = mitr.integerToRoman(num);
		System.out.println(romanStr);
	}

	public String integerToRoman(int num) {

		String[] symbol = { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" };
		int[] value = { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };
		String res = "";

		for (int i = 0; num > 0;) {
			for (int j = 0, k = num / value[i]; j < k; j++) {
				res += symbol[i];
			}
			 num %= value[i];
			 i++;
		}

		return res;
	}
}
