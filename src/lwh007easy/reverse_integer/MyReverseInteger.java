package lwh007easy.reverse_integer;

import java.util.Stack;

public class MyReverseInteger {

	public static void main(String[] args) {

		MyReverseInteger mri=new MyReverseInteger();
		int x=1534236469;//-123;//-2147483648;
//		System.out.println(9646324351>Integer.MAX_VALUE||-2147483648==Integer.MIN_VALUE);
		int result=mri.reverse(x);
	}

	public int reverse(int x) {
		
		String resStr="";
		long resNum=0;
		int mark=1;
		if(x==0||x<=Integer.MIN_VALUE||x>=Integer.MAX_VALUE) {
			return 0;
		}
		if(x<0) {
			mark=-1;
			x=Math.abs(x);
		}
		
		String s=Integer.toString(x);
		Stack<String> stack=new Stack<>();
		//add all nums in stack
		for(int i=0;i<s.length();i++) {
			stack.push(s.substring(i, i+1));
		}
		//mark checking
		if(mark<0) {
			stack.push("-");
		}
		//take nums out of stack and make it a num
		while(!stack.isEmpty()) {
			resStr+=stack.pop();
		}
		resNum=Long.parseLong(resStr);
		System.out.println(resNum);
		if(resNum>Integer.MAX_VALUE||resNum<Integer.MIN_VALUE) {
			return 0;
		}else {
			return (int)resNum;
		}
	}

}
