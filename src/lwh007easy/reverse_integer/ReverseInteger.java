package lwh007easy.reverse_integer;

import java.util.Scanner;

/*
 * int类型的数字反转
 *Given a 32-bit signed integer, reverse digits of an integer.

Example 1:

Input: 123
Output:  321
Example 2:

Input: -123
Output: -321
Example 3:

Input: 120
Output: 21
Note:
Assume we are dealing with an environment which could only hold 
integers within the 32-bit signed integer range. For the purpose 
of this problem, assume that your function returns 0 when the reversed integer overflows.
 */
/*
 * -2^32<int<2^32-1
 * 
 * -31%10=-1    31%10=1
 * 1%10=1 		0%10=0
 */
public class ReverseInteger {

	
	public static void main(String[] args) {

		ReverseInteger ri=new ReverseInteger();
		Scanner sc=new Scanner(System.in);
		
		int inputNum=0;
		int reversedNum=0;
		String inputStr=sc.nextLine();
		inputNum=Integer.parseInt(inputStr);
		
		reversedNum=ri.reverseInteger(inputNum);		
		System.out.println(reversedNum);
		
		sc.close();
	}

	/**
	 * @param inputNum
	 * @return
	 */
	private int reverseInteger(int inputNum) {
		long result=0;	//use long to avoid overflow result
		int positiveMark=1;	//actually positive and negative mark do not affect calculation
		
		if(inputNum==0||inputNum>=Integer.MAX_VALUE||inputNum<=Integer.MIN_VALUE){
			return 0;
		}else if(inputNum<0){
			//to prove the mark problem, remove whole if statement
			positiveMark=-1;
			inputNum=Math.abs(inputNum);
		}
		
		while(inputNum!=0){
			if(result>Integer.MAX_VALUE){
				return 0;
			}
			result=result*10+inputNum%10;
			inputNum=inputNum/10;
		}
		
		if(result>Integer.MAX_VALUE){
			return 0;
		}else{
			return (int) (result*positiveMark);
		}
	}

}

/*
 * Input:
	-2147483648
 * Output:
	-126087180
 */
