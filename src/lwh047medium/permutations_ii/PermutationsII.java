package lwh047medium.permutations_ii;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
Given a collection of numbers that might contain duplicates, 
return all possible unique permutations.

For example,
[1,1,2] have the following unique permutations:
[
  [1,1,2],
  [1,2,1],
  [2,1,1]
]
 */

public class PermutationsII {
	
	List<List<Integer>> list=new ArrayList<List<Integer>>();
	
	public static void main(String[] args) {
		PermutationsII p=new PermutationsII();
		
		int[] nums={1,1,2};
		
		List<List<Integer>> result=p.permuteUnique(nums);
	}

	private List<List<Integer>> permuteUnique(int[] nums) {
		int length=nums.length;	
		if(nums==null||length==0){
			return list;
		}
		
		boolean[] used=new boolean[length];
		List<Integer> rsList=new ArrayList<Integer>();
		
		Arrays.sort(nums);
		deepFirstSearch(nums,used,rsList,length);
		
		return list;
	}

	private void deepFirstSearch(int[] nums, boolean[] used, List<Integer> rsList, int length) {
		if(rsList.size()==length){
			list.add(new ArrayList<Integer>(rsList));
			return;
		}
		
		for(int i=0;i<length;i++){

            if(used[i]){
            	continue;
            }

            if(i>0&&nums[i]==nums[i-1]&&!used[i-1]){

            	continue;
            }
            used[i]=true;
            rsList.add(nums[i]);
            deepFirstSearch(nums, used, rsList, length);
            rsList.remove(rsList.size()-1);
            used[i]=false;
		}
	}

}
