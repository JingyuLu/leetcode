package lwh079medium.word_search;

import java.util.List;

import lwh078medium.subsets.Subsets;

public class WordSearch {
	public static void main(String[] args) {
		
		WordSearch ws=new WordSearch();
		
		char[][] board= {
				{'A','B','C','E'},
				{'S','F','C','S'},
				{'A','D','E','E'}
		};
		String word="ABCCED";
		
		boolean result=ws.exist(board,word);
	}

	public boolean exist(char[][] board, String word) {
		
		for(int i=0;i<board.length;i++) {
			for(int j=0;j<board[0].length;j++) {
				if(board[i][j]==word.charAt(0)) {
					if(verifyLetter(board,word,i,j,0)==true) {
						return true;
					}
				}
			}
		}
		
		return false;
	}

	public boolean verifyLetter(char[][] board, String word, int i, int j, int index) {
		
		if(index==word.length()) {
			return true;
		}
		if(i<0||j<0||i>=board.length||j>=board[0].length||board[i][j]!=word.charAt(index)) {
			return false;
		}
		char temp=board[i][j];
		board[i][j]='#';	
		boolean res=verifyLetter(board, word, i-1, j, index+1)||verifyLetter(board, word, i, j-1, index+1)||verifyLetter(board, word, i+1, j, index+1)||verifyLetter(board, word, i, j+1, index+1);
		board[i][j]=temp;
		
		return res;
	}
}
