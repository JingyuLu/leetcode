package lwh032hard.longest_valid_parentheses;

import java.util.Stack;

public class MyLongestValidParentheses {

	public static void main(String[] args) {

		MyLongestValidParentheses lvp=new MyLongestValidParentheses();
		String s="(()";
		
		int res=lvp.longestValidParentheses(s);
		System.out.println(res);
	}

	private int longestValidParentheses(String s) {
		
		Stack<Integer> st = new Stack<Integer>();
        for(int i = 0 ; i < s.length() ; i++){
            if(s.charAt(i)==')'){
                if(!st.isEmpty() && s.charAt(st.peek())=='('){
                    st.pop();
                    continue;
                }
            }
            st.push(i);
        }
        int maxLength = 0;
        int nextIndex = s.length();
        while(!st.isEmpty()){
            int curIndex = st.pop();
            int curLength = nextIndex-curIndex-1;
            maxLength = curLength>maxLength ? curLength : maxLength;
            nextIndex = curIndex;
        }
        return Math.max(nextIndex, maxLength);
	}

}
