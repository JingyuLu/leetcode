package lwh032hard.longest_valid_parentheses;

public class LongestValidParentheses {

	public static void main(String[] args) {

		LongestValidParentheses lvp=new LongestValidParentheses();
		String s="(()";
		
		int res=lvp.longestValidParentheses(s);
		System.out.println(res);
	}

	private int longestValidParentheses(String s) {
		
		int len=s.length();
		int count=0;
		int start;
		int endMark=0;
		
		if(s.contains("(")) {
			//s=")"
			start=s.indexOf("(");
		}else {
			return 0;
		}
		if(start==len-1) {
			return 0;
		}
		for(int i=start;i<len;i++) {
			char tmp=s.charAt(i);
			if(tmp=='(') {
				count++;
				endMark++;
			}else {
				count++;
				endMark--;
			}
			if(endMark<0) {
				count--;
				break;
			}
		}		
		if(endMark<0) {
			return count;
		}else {
			return count-endMark;
		}
	}

}
