package lwh101easy.symmetric_tree;

public class SymmetricTree {

	public static void main(String[] args) {
		
		SymmetricTree st=new SymmetricTree();
		TreeNode root=new TreeNode(1);
		
		boolean result=st.isSymmetric(root);
	}

	public boolean isSymmetric(TreeNode root) {
		
		if(root==null) {
			return true;
		}
		
		
		return childValidation(root.left,root.right);
	}

	public boolean childValidation(TreeNode rootL, TreeNode rootR) {
		
		if(rootL==null&&rootR==null) {
			return true;
		}
		if(rootL==null||rootR==null) {
			return false;
		}
		if(rootL.val!=rootR.val) {
			return false;
		}
		
		return childValidation(rootL.left, rootR.right)&&childValidation(rootL.right, rootR.left);
	}

}
class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	public TreeNode(int x) {
		val = x;
	}
}