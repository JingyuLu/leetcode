package lwh055medium.jump_game;

public class JumpGame {

	public static void main(String[] args) {
		
		JumpGame jg=new JumpGame();
		
		int[] nums= {3,2,1,0,4};
		
		boolean result=jg.canJump(nums);
		System.out.println(result);
	}

	private boolean canJump(int[] nums) {
		
		int length=nums.length;
		int maxCover=0;	
		int step=1;	
		if(length==0) {
			return false;
		}
		
		for(int i=0;i<length;i++) {
			step--;	
			if(i+nums[i]>maxCover) {
				maxCover=i+nums[i];
				step=nums[i];
			}
			if(step==0&&i<length-1) {
				return false;
			}
		}
		
		return true;
	}

}

