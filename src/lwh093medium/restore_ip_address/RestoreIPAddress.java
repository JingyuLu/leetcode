package lwh093medium.restore_ip_address;

import java.util.ArrayList;
import java.util.List;

public class RestoreIPAddress {

	public static void main(String[] args) {

		RestoreIPAddress rip = new RestoreIPAddress();
		String s = "0666";

		List<String> result = rip.restoreIpAddresses(s);
		
		System.out.println(result);
	}

	public List<String> restoreIpAddresses(String s) {
		List<String> res = new ArrayList<>();
		if (s == null || s.length() < 4 || s.length() > 12) {
			return res;
		}
		dfs(s, "", res, 1);
		return res;
	}

	public void dfs(String s, String temp, List<String> res, int count) {

		if (count == 4 && isValid(s)) {
			res.add(temp + s);
			return;
		}
		for (int i = 1; i < Math.min(4, s.length()); i++) {
			String curs = s.substring(0, i);
			if (isValid(curs)) {
				dfs(s.substring(i), temp + curs + ".", res, count + 1);
			}
		}
	}

	public boolean isValid(String s) {

		if (s.charAt(0) == '0') {
			return s.equals("0");
		}
		int num = Integer.parseInt(s);

		return 0 < num && num < 256;
	}
}
