package lwh020easy.valid_parentheses;

import java.util.HashMap;
import java.util.Stack;

public class ValidParentheses {

	public boolean isValid(String s) {
		
//		  //solution1 stack
//        Stack<String> stringStack=new Stack<String>();
//        Stack<String> testStack=new Stack<String>();
//        
//        for(int i=0;i<s.length();i++){
//        	String temp=s.substring(i, i+1);
//        	stringStack.push(temp);
//        }
//        
//        /*
//         * attention only if stack is null isEmpty() return true; not null return false
//         */
//        while(!stringStack.isEmpty()){
//        	// first in last come, so first come out bracket is ) second (
//        	String bottomBracket=stringStack.peek();
//        	String topBracket=stringStack.peek();
//        	testStack.push(topBracket);
////        	System.out.println("top   "+topBracket);
////        	System.out.println("bottom   "+bottomBracket);
//        	if(topBracket.equalsIgnoreCase("(")){
//        		if(bottomBracket.equalsIgnoreCase(")")){
////        			testStack.pop();	//test stack is empty now,so wrong
//        			stringStack.pop();
//        			stringStack.pop();
//        		}else{
//        			testStack.push(bottomBracket);
//        		}
//        	}else if(topBracket.equalsIgnoreCase("[")){
//        		if(bottomBracket.equalsIgnoreCase("]")){
////        			testStack.pop();
//        			stringStack.pop();
//        			stringStack.pop();
//        		}else{
//        			testStack.push(bottomBracket);
//        		}
//        	}else if(topBracket.equalsIgnoreCase("{")){
//        		if(bottomBracket.equalsIgnoreCase("}")){
////        			testStack.pop();
//        			stringStack.pop();
//        			stringStack.pop();
//        		}else{
//        			testStack.push(bottomBracket);
//        		}
//        	}else{
//        		System.out.println("Wrong Input!");
//        	}
//        }
//        
////        System.out.println(testStack.isEmpty()+"  "+testStack.peek()+"666");
//        
//        if(testStack.isEmpty()){
//        	System.out.println("true");
//        	return true;
//        }else{
//        	System.out.println("false");
//        	return false;
//        }
		
//		//solution 2
//		HashMap<Character, Character> map = new HashMap<Character, Character>();
//		map.put('(', ')');
//		map.put('[', ']');
//		map.put('{', '}');
//	 
//		Stack<Character> stack = new Stack<Character>();
//	 
//		for (int i = 0; i < s.length(); i++) {
//			char curr = s.charAt(i);
//	 
//			if (map.keySet().contains(curr)) {
//				stack.push(curr);
//			} else if (map.values().contains(curr)) {
//				if (!stack.empty() && map.get(stack.peek()) == curr) {
//					stack.pop();
//				} else {
//					return false;
//				}
//			}
//		}
//	 
//		return stack.empty();
		
		//solution 3
		Stack<Character> stack=new Stack<Character>();
		
		if(s.length()==0||s.length()%2!=0){
			return false;
		}
		for(int i=0;i<s.length();i++){
			if(s.charAt(i)=='('||s.charAt(i)=='['||s.charAt(i)=='{'){
				stack.push(s.charAt(i));
			}else{
				if(stack.size()==0){
					return false;
				}
				char top=stack.pop();
				
				if(s.charAt(i)==')'&&top!='('){
					return false;
				}else if(s.charAt(i)==']'&&top!='['){
					return false;
				}else if(s.charAt(i)=='}'&&top!='{'){
					return false;
				}
			}
		}
		return stack.size()==0;
    }
    
    public static void main(String[] args){
    	ValidParentheses vp=new ValidParentheses();
    	String s="([]{})";
    	boolean b=vp.isValid(s);
    	System.out.println(b);
    }

}
