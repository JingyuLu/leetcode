package lwh003medium.longest_substring_without_repeating_characters;

import java.util.HashMap;

/*
Given a string, find the length of the longest substring without repeating characters.

Examples:

Given "abcabcbb", the answer is "abc", which the length is 3.

Given "bbbbb", the answer is "b", with the length of 1.

Given "pwwkew", the answer is "wke", with the length of 3.

Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
 */
public class LongestSubstringWithoutRepeatingCharacters {

	public static void main(String[] args) {
		
		LongestSubstringWithoutRepeatingCharacters ll=new LongestSubstringWithoutRepeatingCharacters();
		
//		String inputStr="abcabcbb";	//3
		String inputStr="pwwkew";	//3
//		String inputStr="au";	//2
//		String inputStr="dvdf";	//2
		
		
		int result=ll.findSubString(inputStr);
		
		System.out.println(result);
	}

	/**
	 * @param inputStr
	 * @return
	 */
	private int findSubString(String s) {
	
//		//solution wrong
//		if(s==null){
//			return 0;
//		}else if(s.length()==1){
//			return 1;
//		}
//		
//		String tempString;
//		String subString;
//		int strLength=s.length();
//		int curLength=0;
//		int maxLength=0;
//		int slow=0;
//		int fast=0;
//		
//
//		for(fast=1;fast<strLength;fast++){
//			subString=s.substring(slow, fast);
//			tempString=s.substring(fast,fast+1);
//			
//			if(subString.indexOf(tempString)==0){
//				slow++;
//				curLength=subString.length();				
//			}else if(subString.indexOf(tempString)>0){
//				slow=subString.indexOf(tempString)+1;
//			}else{
//				curLength=subString.length()+1;
//			}
//			
//			if(maxLength<curLength){
//				maxLength=curLength;
//			}
//			System.out.println(subString+" "+slow+" "+fast);
//		}
//		
//		return maxLength;
		
		//solution 2

			/*
			    "滑动窗口" 
			    比方 abcabccc 当你右边扫描到abca的时候你得把第一个a删掉得到bca
			    然后"窗口"继续向右滑动，每当加到一个新char的时候，左边查有无重复的char�?
			    然后如果没有重复的就正常添加
			    有重复的话就左边扔掉部分（从左到重复char这段扔掉），在这个过程中记录大窗口长
			*/
		if(s==null){
			return 0;
		}
		
		HashMap<Character, Integer> map=new HashMap<Character,Integer>();
		
		int leftBound=0;
		int maxLength=0;
		int curVal=0;
		
		for(int i=0;i<s.length();i++){
			char tempChar=s.charAt(i);
			if(map.containsKey(tempChar)){
				curVal=map.get(tempChar);
				leftBound=Math.max(leftBound, curVal+1);
			}else{
				curVal=0;
				leftBound=Math.max(leftBound, curVal);
			}
			maxLength=Math.max(maxLength, i-leftBound+1);
			map.put(tempChar, i);
		}
		return maxLength;
		
	}

}
