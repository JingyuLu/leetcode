package lwh003medium.longest_substring_without_repeating_characters;

import java.util.HashMap;

public class MyLongestSubString {

	/*
	 * subString 子字符串，注意字符串必须连续
	 * subSequence 子序列，注意子序列可以不连续
	 * 
	 * Input: "pwwkew"
	 * Output: 3
	 * Explanation: The answer is "wke", with the length of 3. 
             Note that the answer must be a substring, 
             "pwke" is a subsequence and not a substring.
	 */
	public static void main(String[] args) {
		MyLongestSubString mlss=new MyLongestSubString();
		String s="abcabcbb";
		
		int result=mlss.lengthOfLongestSubstring(s);
		System.out.println(result);
	}

	public int lengthOfLongestSubstring(String s) {
		
		if(s==null||s.length()==0) {
			return 0;
		}
		
		HashMap<Character, Integer> map=new HashMap<Character, Integer>();
		int max=0;
		for(int fast=0,slow=0;fast<s.length();fast++) {
			if(map.containsKey(s.charAt(fast))) {
				//更新指针j的位置，j指向重复字符的下一个字符-将重复字符移除子串
				slow=Math.max(slow, map.get(s.charAt(fast))+1);
			}
			map.put(s.charAt(fast), fast);	//将新的字符存入map
			max=Math.max(max, fast-slow+1);
		}
		return max;
	}

}
