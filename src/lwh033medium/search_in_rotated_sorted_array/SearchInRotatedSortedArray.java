package lwh033medium.search_in_rotated_sorted_array;

public class SearchInRotatedSortedArray {

	public static void main(String[] args) {
		SearchInRotatedSortedArray s=new SearchInRotatedSortedArray();
		int[] nums={3,1};
		int target=1;
		
		int result=s.searchTarget(nums,target);
		System.out.println(result);
	}

	private int searchTarget(int[] nums, int target) {
		
		int min=0;
		int max=nums.length-1;
		int mid=0;
		
		while(min<=max){
			mid=(min+max)/2;
			if(nums[mid]==target){
				return mid;
			}

			if(nums[mid]<=target){
				if(nums[mid]<=target&&target<nums[mid]){
					max=mid-1;
				}else{
					min=mid+1;
				}
			}else{

				if(nums[mid] < target && target <= nums[max]){
					mid=mid+1;
				}else{
					max=mid-1;
				}
			}		
		}
		return -1;
	}

}
