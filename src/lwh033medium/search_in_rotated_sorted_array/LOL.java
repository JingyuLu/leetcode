package lwh033medium.search_in_rotated_sorted_array;

public class LOL {

	public static void main(String[] args) {
		
		LOL lol=new LOL();
		int[] nums={0,1,2,4,5,6,7};
		int target=6;
		
		int result=lol.searchTarget(nums,target);
	}

	private int searchTarget(int[] nums, int target) {
		
		int k=-1;
		for(int i=0;i<nums.length;i++) {
			if(nums[i]==target) {
				k=i;
				break;
			}
		}
		return k;
	}

}
