package lwh035easy.search_insert_position;


public class SearchInsertPosition {

	public static void main(String[] args) {

		SearchInsertPosition sip=new SearchInsertPosition();
		
		int[] nums={1,3,5,6};
		int target=5;
		
		int result=sip.searchInsertPosition(nums,target);
		System.out.println(result);

	}

	private int searchInsertPosition(int[] nums, int target) {

		int position=-1;
		
		for(int i=0;i<nums.length;i++){
			if(nums[i]==target){
				position=i;
			}
		}
		
		if(position==-1){
			if(target<nums[0]){
				position=0;
			}else{
				for(int j=0;j<nums.length;j++){
					if(nums[j]<target){
						position=j+1;
					}
				}
			}
		}
		return position;
	}

}
