package lwh089medium.gray_code;

import java.util.ArrayList;
import java.util.List;

public class GrayCode {

	public static void main(String[] args) {
			
		GrayCode gc=new GrayCode();
		int n=2;
		
		List<Integer> result=gc.grayCode(n);
	}

	public List<Integer> grayCode(int n) {
		
		List<Integer> res=new ArrayList<>();
		if(n==0) {
			res.add(0);
			return res;
		}
		
		List<Integer> list=grayCode(n-1);
		int originSize=list.size();
		int addNum=1<<(n-1);
		System.out.println(addNum);
		System.out.println("+++++++++++++++++++++++++");
		for(int i=originSize-1;i>=0;i--) {
			list.add(addNum+list.get(i));
			System.out.println(addNum);
		}
		return list;
	}

}
