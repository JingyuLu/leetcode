package lwh048medium.rotate_image;

/*
You are given an n x n 2D matrix representing an image.

Rotate the image by 90 degrees (clockwise).

Note:

You have to rotate the image in-place, which means you have to modify the input 2D matrix directly. DO NOT allocate another 2D matrix and do the rotation.

Example 1:

Given input matrix = 
[
  [1,2,3],
  [4,5,6],
  [7,8,9]
],

rotate the input matrix in-place such that it becomes:
[
  [7,4,1],
  [8,5,2],
  [9,6,3]
]
Example 2:

Given input matrix =
[
  [ 5, 1, 9,11],
  [ 2, 4, 8,10],
  [13, 3, 6, 7],
  [15,14,12,16]
], 

rotate the input matrix in-place such that it becomes:
[
  [15,13, 2, 5],
  [14, 3, 4, 1],
  [12, 6, 8, 9],
  [16, 7,10,11]
]
 */
public class RotateImage {

	public void printMatrix(int[][] matrix) {
		for(int[] i:matrix) {
			for(int j:i) {
				System.out.print(j+"  ");
			}
			System.out.println();
		}
		System.out.println("++++++++++");
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		RotateImage ri=new RotateImage();
		
//		int[][] matrix={
//				{5,1,9,11},
//				{2,4,8,10},
//				{13,3,6,7},
//				{15,14,12,16}
//		};
		
		int[][] matrix={
				{1,2,3,4},
				{5,6,7,8},
				{9,10,11,12},
				{13,14,15,16}
		};
		
		ri.rotateImage(matrix);
		
		ri.printMatrix(matrix);
	}

	/*

1 2 3 => 7 8 9 => 7 4 1
4 5 6 => 4 5 6 => 8 5 2
7 8 9 => 1 2 3 => 9 6 3

1  2  3  4			13 14 15 16			13 9  5 1
5  6  7  8			9  10 11 12			14 10 6	2
9  10 11 12			5  6  7  8			15 11 7 3
13 14 15 16			1  2  3  4			16 12 8 4
	 */
	private void rotateImage(int[][] matrix) {
		int length=matrix.length;
//		System.out.println(length);

		for(int i=0;i<length/2;i++){
			for(int j=0;j<length;j++){
				int temp=matrix[i][j];
				matrix[i][j]=matrix[length-1-i][j];
				matrix[length-1-i][j]=temp;
			}
		}
		
		printMatrix(matrix);

//		for(int i=0;i<length;i++){
//			for(int j=0;j<length-i;j++){
//				int temp=matrix[i][j];
//				matrix[i][j]=matrix[length-1-j][length-1-i];
//				matrix[length-1-j][length-1-i]=temp;
//			}
//		}
		

		for (int i = 0; i < length; i++) {
			for (int j = i+1; j < length; j++) {
				int temp = matrix[i][j];
				matrix[i][j] = matrix[j][i];
				matrix[j][i] = temp;
			}
		}
		
	}
}
