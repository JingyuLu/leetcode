package lwh074medium.search_2D_matrix;


public class Search2DMatrix {

	public static void main(String[] args) {
		
		Search2DMatrix sm=new Search2DMatrix();
		
//		int[][] matrix= {
//				{1,   3,  5,  7},
//				{10, 11, 16, 20},
//				{23, 30, 34, 50}
//		};
		int[][] matrix= {{3}};
		int target=3;
		
		boolean result=sm.searchMatrix(matrix,target);
		System.out.println(result);
	}

	public boolean searchMatrix(int[][] matrix, int target) {
		
		if(matrix.length==0||matrix[0].length==0) {
//			System.out.println("1");
			return false;
		}
		int flag=0;
		int row=matrix.length;
		int col=matrix[0].length;
		if(target<matrix[0][0]||target>matrix[row-1][col-1]) {
//			System.out.println("2");
			return false;
		}
		if(target>=matrix[row-1][0]) {
			if(match(matrix[row-1],target)) {
//				System.out.println("3");
				return true;
			}
		}
		for(int i=0;i<row-1;i++) {
			if(target>=matrix[i][0]&&target<matrix[i+1][0]) {
				if(match(matrix[i],target)) {
//					System.out.println("4");
					return true;
				}
			}
		}
//		System.out.println("5");
		return false;
	}

	public boolean match(int[] is, int target) {
		
		for(int i=0;i<is.length;i++) {
			if(is[i]==target) {
				return true;
			}
		}
		return false;
	}

}
