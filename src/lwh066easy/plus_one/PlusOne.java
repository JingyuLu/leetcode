package lwh066easy.plus_one;

public class PlusOne {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PlusOne po=new PlusOne();
		
		int[] digits={6,9};
		
		int[] nums=po.plusOne(digits);
		
//		int[] test=new int[6];
//		test[0]=1;
		for(int i:nums){
			System.out.print(i+" ");
		}
		
		
	}

	/**
	 * @param nums
	 */
	private int[] plusOne(int[] digits) {
		
		int length=digits.length;
		for(int i=length-1;i>=0;i--){
			if(digits[i]<9){
				digits[i]=digits[i]+1;
				return digits;
			}else{
				digits[i]=0;
			}
		}
		int[] newArray=new int[length+1];
		newArray[0]=1;	
		return newArray;
	}

}
