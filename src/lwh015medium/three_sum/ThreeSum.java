package lwh015medium.three_sum;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
Given an array S of n integers, are there elements a, b, c in S such that a + b + c = 0? Find all unique triplets in the array which gives the sum of zero.

Note: The solution set must not contain duplicate triplets.

For example, given array S = [-1, 0, 1, 2, -1, -4],

A solution set is:
[
  [-1, 0, 1],
  [-1, -1, 2]
]


由题{-1,0,1}和{1,0，-1}是一种情况
 */
public class ThreeSum {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ThreeSum ts=new ThreeSum();
		int[] array={};
		
		List list=ts.threeSum(array);
	}


	/**
	 * @param array
	 * @return
	 */
	private List<List<Integer>> threeSum(int[] nums) {
		//三数求和
		Arrays.sort(nums);	//将数组排序
		ArrayList<List<Integer>> res=new ArrayList<List<Integer>>();
		for(int i=0;i<nums.length-2;i++){
			//跳过重复元素,第一次遇到时计算，下次再遇到相同的数值则跳过
			if(i>0&&nums[i]==nums[i-1]){
				continue;
			}else{
				//转换为两数求和
				ArrayList<List<Integer>> cur=twoSum(nums,i,0-nums[i]);
				res.addAll(cur);
				
			}
		}	
		return res;
	}


	/**
	 * @param nums
	 * @param i
	 * @param j
	 * @return
	 */
	private ArrayList<List<Integer>> twoSum(int[] nums, int i, int target) {

		int left=i+1;
		int right=nums.length-1;
		ArrayList<List<Integer>> res=new ArrayList<List<Integer>>();
		
		while(left<right){
			if(nums[left]+nums[right]==target){
				ArrayList<Integer> cur=new ArrayList<Integer>();
				cur.add(nums[i]);
				cur.add(nums[left]);
				cur.add(nums[right]);
				res.add(cur);
				//去除重复的计算
				do{
					left++;
				}while(left < nums.length && nums[left] == nums[left-1]);
//				while(left < right && nums[left] == nums[left-1]);
                do {
                    right--;
                } while(right >= 0 && nums[right] == nums[right+1]);
//                while(left<right && nums[right] == nums[right+1]);
            } else if (nums[left] + nums[right] > target){
                right--;
            } else {
                left++;
            }
        }
        return res;
	}

}
