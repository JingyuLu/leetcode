package lwh015medium.three_sum;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MyThreeSum {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyThreeSum mts=new MyThreeSum();
		
		int[] nums= {-1, 0, 1, 2, -1, -4};//{-4,-1,-1,0,1,2}
		
		List<List<Integer>> list=mts.threeSum(nums);
		System.out.println(list.toString());
	}

	public List<List<Integer>> threeSum(int[] nums) {
		
		List<List<Integer>> res=new ArrayList<List<Integer>>();
		Arrays.sort(nums);
		for(int i=0;i<nums.length;i++) {
			if(i>0&&nums[i]==nums[i-1]){
				continue;
			}else{
				List<List<Integer>> list=threeSumCalculation(nums,i,0-nums[i]);
				res.addAll(list);
				
			}
		}
		
		return res;
	}

	public List<List<Integer>> threeSumCalculation(int[] nums, int start, int target) {
		
		List<List<Integer>> sum=new ArrayList<List<Integer>>();
//		wrong duplicated result!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//		for(int i=start+1;i<nums.length-2;i++) {
//			if(nums[i]+nums[i+1]==target) {
//				List<Integer> list=new ArrayList<>();
//				list.add(nums[start]);
//				list.add(nums[i]);
//				list.add(nums[i+1]);
//				sum.add(list);
//				System.out.println(i+" "+nums[i]+" "+nums[i+1]);
//			}
//		}
		
		for (int i = start + 1; i < nums.length - 2; i++) {
			for(int j=i+1;j<nums.length;j++) {
				if (nums[i] + nums[j] == target) {
					List<Integer> list = new ArrayList<>();
					list.add(nums[start]);
					list.add(nums[i]);
					list.add(nums[j]);
					sum.add(list);
				}
			}
		}
		
		return sum;
	}

}
