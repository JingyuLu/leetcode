package lwh027easy.remove_element;

import java.awt.image.ImageProducer;
import java.util.Scanner;

public class RemoveElement {
	
	public static void main(String[] args) {
		
		RemoveElement re=new RemoveElement();
		Scanner sc=new Scanner(System.in);
		
		int[] inputArray={3,2,2,2,3};
		int inputNum=sc.nextInt();
		sc.nextLine();
		
		int newBound=re.removeElement(inputArray,inputNum);
		
		for(int i=0;i<newBound;i++){
			System.out.print(inputArray[i]+" ");
		}
	}

	
	private int removeElement(int[] nums, int val) {
		
		int slow=0;
		for(int fast=0;fast<nums.length;fast++){
			if(nums[fast]!=val){
				nums[slow]=nums[fast];
				slow++;
			}
		}
		return slow;	
	}

}
