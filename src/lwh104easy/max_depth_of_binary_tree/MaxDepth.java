package lwh104easy.max_depth_of_binary_tree;

public class MaxDepth {

	public static void main(String[] args) {
		MaxDepth md = new MaxDepth();
		TreeNode root=new TreeNode(1);
		
		int result=md.maxDepth(root);
	}

	public int maxDepth(TreeNode root) {
		
		if(root==null) {
			return 0;
		}
		int depthL=maxDepth(root.left);
		int depthR=maxDepth(root.right);
		return (depthL>depthR?depthL:depthR)+1;
	}
}

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}