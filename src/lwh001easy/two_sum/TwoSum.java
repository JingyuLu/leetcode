package lwh001easy.two_sum;

import java.util.Scanner;

/*
 * �������
Given an array of integers, return indices of the two numbers such that they add up to a specific target.
You may assume that each input would have exactly one solution, and you may not use the same element twice.

Example:
Given nums = [2, 7, 11, 15], target = 9,
Because nums[0] + nums[1] = 2 + 7 = 9,
return [0, 1].

 */
public class TwoSum {

	int[] result=new int[2];
	
	public int[] twoSum(int[] nums, int target) {
//		System.out.println("=================");
        for(int i=0;i<nums.length;i++){
//        	System.out.println("=================");
            for(int j=i+1;j<nums.length+1;j++){
//            	System.out.println("=================");
                if(nums[i]+nums[j]==target){
//                	System.out.println("=================");
                    result[0]=i;
                    result[1]=j;
                    return result;
                }
            }
        }
        throw new IllegalArgumentException("No two sum solution");
	}
	
	public static void main(String[] args){
		int[] testIntArray={2, 7, 11, 15};
		
		TwoSum ts=new TwoSum();
		
		int[] result=ts.twoSum(testIntArray,9);
		for(int tempNum:result){
			System.out.print(tempNum+" ");
		}
	}
}
