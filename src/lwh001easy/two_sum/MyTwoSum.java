package lwh001easy.two_sum;

public class MyTwoSum {

	public static void main(String[] args) {
		
		MyTwoSum mts=new MyTwoSum();
		int[] nums= {1,2,4};//{2, 7, 11, 15};
		int target=6;
		
		int[] result=mts.twoSum(nums,target);
	}

	public int[] twoSum(int[] nums, int target) {
		
		int[] res=new int[2];
		validTwoSum(nums,res,0,target);
		return res;
	}

	public void validTwoSum(int[] nums, int[] res, int startIndex, int target) {
		
		if(startIndex>=nums.length-1) {
			return;
		}
		for(int i=startIndex+1;i<nums.length;i++) {	//i should 1 more larger than startIndex when start calculate
			System.out.println(startIndex);
			if(nums[startIndex]+nums[i]==target) {
				res[0]=startIndex;
				res[1]=i;
				System.out.println(res[0]+" "+res[1]);
			}
		}
		validTwoSum(nums, res, startIndex+1, target);
	}

}
