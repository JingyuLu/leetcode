package lwh061medium.rotate_list;

public class RotateList {

	public static void main(String[] args) {
		
		RotateList rl=new RotateList();
		
		ListNode listNode=new ListNode(0);
		listNode.next=new ListNode(1);
		listNode.next.next= new ListNode(2);
		int k=4;
		
		ListNode result=rl.rotateRight(listNode,k);
	}

	private ListNode rotateRight(ListNode head, int k) {
		//avoid null list or single-element list
		if(head==null||head.next==null) {
			return head;
		}
		
		int length=0;
		ListNode fast=head;
		ListNode slow=head;
		ListNode lenNode=head;
		ListNode tempNode=new ListNode(-1);
		
		while(lenNode!=null) {
			length++;
			lenNode=lenNode.next;
		}
		
		k=k%length;
		if(k==0) {
			return head;
		}
		
		for(int i=0;i<k;i++) {
			fast=fast.next;
		}

		while(fast.next!=null) {
			slow=slow.next;
			fast=fast.next;
		}
		
		tempNode=slow.next;
		fast.next=head;
		slow.next=null;
		
		return tempNode;
	}

}

class ListNode{
	int value;
	ListNode next;
	public ListNode(int x) {
		value=x;
	}
}
