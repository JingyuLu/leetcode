package lwh092medium.reverse_linked_list2;

public class ReverseLinnkedList2 {

	public static void main(String[] args) {

		ReverseLinnkedList2 rll2 = new ReverseLinnkedList2();
		ListNode head = new ListNode(1);
		head.next = new ListNode(2);
		head.next.next = new ListNode(3);
		int m = 2;
		int n = 4;

		ListNode result = rll2.reverseBetween(head, m, n);
	}

	private ListNode reverseBetween(ListNode head, int m, int n) {

		if (head == null || head.next == null || m >= n) {
			return head;
		}
		ListNode dummyNode = new ListNode(0);
		dummyNode.next = head;
		head = dummyNode;

		ListNode preNode = head; // pre node

		for (int i = 1; i < m; i++) {
			preNode = preNode.next;
		}

		ListNode nodeA = preNode.next;
		ListNode nodeB = preNode.next.next;

		for (int i = 0; i < n - m; i++) {
			nodeA.next = nodeB.next;
			nodeB.next = preNode.next;
			preNode.next = nodeB;
			nodeB = nodeA.next;
		}

		return dummyNode.next;
	}

}

class ListNode {
	int val;
	ListNode next;

	ListNode(int x) {
		val = x;
	}
}
