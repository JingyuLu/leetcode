package lwh009easy.palindrome_number;

public class MyPalindromeNumber {

	public static void main(String[] args) {
		
		MyPalindromeNumber mpn=new MyPalindromeNumber();
		
		int x=1234554321;//-121;
		
		boolean result=mpn.isPalindrome(x);
		System.out.println(result);
	}

	public boolean isPalindrome(int x) {
		
		if(x<0) {
			return false;
		}else if(x<10) {
			return true;
		}
		String str=Integer.toString(x);
		int strLen=str.length();
//		System.out.println(str+strLen);
		for(int i=0,j=strLen-1;i<=strLen/2;i++,j--) {	//��ż����odd/2 leave a single num at middle one single num must be PalindromeNumber, even/2 get 2 equal half
														// i<=strLen/2+1 may lead to string out of index in 2 num situation like 66
			if(str.charAt(i)!=str.charAt(j)) {
//				System.out.println("F2"+" "+str.charAt(i)+" "+str.charAt(j));
				return false;
			}
		}
		return true;
	}

}
