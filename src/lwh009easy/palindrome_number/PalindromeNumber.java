package lwh009easy.palindrome_number;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/*
 * 判断回文数
 * Determine whether an integer is a palindrome. Do this without extra space.

click to show spoilers.

Some hints:
Could negative integers be palindromes? (ie, -1)

If you are thinking of converting the integer to string, note the restriction of using extra space.

You could also try reversing an integer. However, if you have solved the problem "Reverse Integer", you know that the reversed integer might overflow. How would you handle such case?

There is a more generic way of solving this problem.
 */
/*
 * 说明：
 * 1.题目要求只能用O(1)的空间，所以不能考虑把它转化为字符串然后reverse比较的方法。
 * 2.在提示中也提到了，如果考虑reverse number的方法，可能造成溢出。故只能使用分离数字的方法
 * 思路：
 * 1.将输入整数转换成倒序的一个整数，再比较转换前后的两个数是否相等，但是这样需要额外的空间开销
 * 2.每次提取头尾两个数，判断它们是否相等，判断后去掉头尾两个数。-分离数字
 */
public class PalindromeNumber {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PalindromeNumber pn=new PalindromeNumber();
		
		Scanner sc=new Scanner(System.in);
		boolean isPalindromeNumber=false;
		int num=sc.nextInt();
		sc.nextLine();
		
		isPalindromeNumber=pn.palindromeNumberValidation(num);
		System.out.println(isPalindromeNumber);
		
		sc.close();
	}

	/**
	 * @param num
	 * @return
	 */
	private boolean palindromeNumberValidation(int x) {
		
		if(x<0){
			//negative number cannot be Palindrome Number
			System.out.println("x<0");
			return false;
		}else if(x<10){
			//single number
			System.out.println("single number");
			return true;
		}
		
		int lengthOfNum=1;	//minimum length is 1, one number only
		int digits=1;
				
		while(digits<=x/10){
			//get digits and length of input number
			digits=digits*10;
			lengthOfNum++;
		}
		System.out.println("digits:"+digits+" "+"length: "+lengthOfNum);
		
//		//solution 1
//		while(x>0){
//			//get the head and tail of number
//			int headNum=x/digits;
//			int tailNum=x%10;
//			
//			if(headNum==0){
//				return false;
//			}else if(headNum!=tailNum){
//				return false;
//			}else{
//				//remove used head and tail of number
//				x=(x%10)/10;	//??????????????????????????????
//				digits=digits/100;	//because last statement removed two digits from number
//			}
//		}	//solution 1 end
		
		//solution 2
		/*
		 * 分别从数的开头结尾各取一半，作比较，如相同则通过
		 */

		digits=(int) (digits/Math.pow(10, lengthOfNum/2));
		int headNum=x/digits;
		int tailNum=0;
		
		for(int j=0;j<=lengthOfNum/2;j++){
			//get reversed tailNum
			tailNum=10*tailNum+x%10;
			x=x/10;
		}
		if(headNum==tailNum){
			System.out.println("head=tail");
			return true;
		}else{
			System.out.println("head!=tail "+headNum+" "+tailNum);
			return false;
		}
	}

}

/*
   Input:
	1000021, 1001
   Output:
	true
   Expected:
	false
	
   Input:
	1001
   Output:
	false
   Expected:
	true
 */
