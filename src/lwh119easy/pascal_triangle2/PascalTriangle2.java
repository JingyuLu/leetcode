package lwh119easy.pascal_triangle2;

import java.util.ArrayList;
import java.util.List;

public class PascalTriangle2 {

	public static void main(String[] args) {
		
		PascalTriangle2 pt2=new PascalTriangle2();
		int rowIndex=3;
		
		List<Integer> result=pt2.getRow(rowIndex);
	}

	private List<Integer> getRow(int rowIndex) {
		
		List<Integer> res=new ArrayList<Integer>(rowIndex+1);
		res.add(1);
		
		if(rowIndex==0) {
			return res;
		}
		
		res.add(1);
		if(rowIndex==1) {
			return res;
		}
		
		for(int i=2;i<=rowIndex;i++) {
			res.add(1);
			for(int j=i-1;j>0;j--) {
				res.set(j, res.get(j)+res.get(j-1));
			}
		}
		
		return res;
	}

}
