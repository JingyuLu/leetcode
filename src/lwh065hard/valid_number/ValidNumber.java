package lwh065hard.valid_number;

/*
 * 推断1个字符串是否是数字。
 */

public class ValidNumber {
wrong	
	public static void main(String[] args) {
		ValidNumber vn=new ValidNumber();
		
		String s="0";
		
		boolean result=vn.isNumber(s);
		System.out.println(result);
	}

	public boolean isNumber(String s) {
		while(s.startsWith(" ")) {

			s=s.substring(1);
		}
		while(s.endsWith(" ")) {
			s=s.substring(0, s.length()-1);
		}
		
		if(s.length()<1) {
			System.out.println("1");
			return false;
		}
		
		if(s.indexOf('e')==-1) {
			return check(s,true);
		}else {
			return check(s.substring(0,s.indexOf('e')),true)&&check(s.substring(s.indexOf('e')+1),false);	
		}
	}

	public boolean check(String tmp, boolean point) {
		if(tmp.length()<1) {
			System.out.println("2");
			return false;
		}
		
		char[] chars=tmp.toCharArray();
		int i=0;
		boolean num=false;
		if(chars[i]=='+'||chars[i]=='-') {
			i++;
		}
		if(i==chars.length) {
			System.out.println("3");
			return false;
		}

		while(i<chars.length&&chars[i]<=9&&chars[i]>='0') {
			i++;
			num=true;
		}
		if(i<chars.length&&point==false) {
			System.out.println("4");
			return false;
		}
		if(i<chars.length&&chars[i]=='.') {
			boolean num2=false;
			i++;
			while(i<chars.length && chars[i]<='9' && chars[i]>='0') {
				i++;
				num2=true;
			}
			if(num2==false&&num==false) {
				System.out.println("5");
				return false;
			}
		}
		System.out.println("6"+" "+i+" "+chars.length);
		return i==chars.length;
	}

}
