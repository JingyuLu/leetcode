package lwh082medium.remove_duplicates_from_sorted_list2;

public class RemoveDuplicated2 {

	public static void main(String[] args) {
		RemoveDuplicated2 rd2=new RemoveDuplicated2();
		
		ListNode head=new ListNode(1);
		head.next=new ListNode(1);
		head.next.next=new ListNode(2);
		head.next.next.next= new ListNode(3);
		
		ListNode result=rd2.deleteDuplicates(head);
	}

	public ListNode deleteDuplicates(ListNode head) {
		
		if(head==null||head.next==null) {
			return head;
		}
		ListNode temp=new ListNode(0);
		temp.next=head;
		head=temp;
		while(head.next!=null&&head.next.next!=null) {
			if(head.next.val==head.next.next.val) {
				int val=head.next.val;
				while (head.next != null && head.next.val == val) {
                    head.next = head.next.next;
                }
			}else {
                head = head.next;
            }
		}
		
		return head;
	}

}

class ListNode{
	int val;
	ListNode next;
	public ListNode(int x) {
		val=x;
	}	
}