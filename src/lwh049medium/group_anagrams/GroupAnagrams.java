package lwh049medium.group_anagrams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupAnagrams {

	public static void main(String[] args) {
		GroupAnagrams ga=new GroupAnagrams();
		
		String[] strs= {"eat", "tea", "tan", "ate", "nat", "bat"};
		
		List<List<String>> list=ga.groupAnagrams(strs);
	}

	private List<List<String>> groupAnagrams(String[] strs) {

		List<List<String>> result=new ArrayList<List<String>>();
		
		if(strs==null||strs.length==0) {
			return result;
		}
		
		Map<String, List<String>> map=new HashMap<>();
		for(String str:strs) {
			char[] c=str.toCharArray();
			Arrays.sort(c);
			String sortedStr=new String(c);
			if(!map.containsKey(sortedStr)) {
				map.put(sortedStr, new ArrayList<String>());
			}
			map.get(sortedStr).add(str);
		}
		for(Map.Entry<String, List<String>> entry:map.entrySet()) {
			if(entry.getValue().size()>=1) {
				result.add(entry.getValue());
			}
		}
		
		return result;
	}

}
