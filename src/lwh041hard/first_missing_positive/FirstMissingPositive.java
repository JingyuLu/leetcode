package lwh041hard.first_missing_positive;

import java.util.Arrays;

public class FirstMissingPositive {

	public static void main(String[] args) {

		FirstMissingPositive fmp=new FirstMissingPositive();
		int[] nums= {7,8,9,11,12};
		
		int result=fmp.firstMissingPositive(nums);
	}

	private int firstMissingPositive(int[] nums) {

		if(nums==null||nums.length==0) {
			return 1;
		}
		
		for(int i=0;i<nums.length;i++) {
			if(nums[i]<=nums.length&&nums[i]>0&&nums[nums[i]-1]!=nums[i]) {
				int temp=nums[nums[i]-1];
				nums[nums[i]-1]=nums[i];
				nums[i]=temp;
				i--;
			}
		}
		for(int i=0;i<nums.length;i++) {
			if(nums[i]!=i+1) {
				return i+1;
			}
		}
		return nums.length+1;
	}

}
