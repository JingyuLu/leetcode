package lwh034medium.search_range;

public class SearchRange {

	public static void main(String[] args) {
		SearchRange sr=new SearchRange();
		int[] nums={5, 7, 7, 8, 8, 10};
		int target=8;
		
		int[] result=sr.searchRange(nums,target);
	}

	private int[] searchRange(int[] nums, int target) {

		int[] result={-1,-1};
		int start=0;
		int end=nums.length-1;
		if(nums.length==0){
			return result;
		}
		while(nums[start]<nums[end]){
			int mid=(start+end)/2;
			if(target<nums[mid]){
				end=mid-1;
			}else if(target>nums[mid]){
				start=mid+1;
			}else{

				if (nums[start] < target) start++;
                if (nums[end] > target) end--;
			}
			System.out.println(start+"��"+end);
		}
		if(target==nums[start]){
			result[0]=start;
			result[1]=end;
		}
		return result;
	}

}
