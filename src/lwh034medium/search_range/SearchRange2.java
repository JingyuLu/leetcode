package lwh034medium.search_range;


public class SearchRange2 {

	public static void main(String[] args) {
		SearchRange2 sr=new SearchRange2();
		int[] nums={};
		int target=0;
		
		int[] result=sr.searchRange(nums,target);
	}

	private int[] searchRange(int[] nums, int target) {

		int left=0;
		int right=nums.length-1;
		int[] result={-1,-1};
		
		while(left<=right){

			if(target==nums[left]){
				result[0]=left;
				break;
			}else{
				left++;
			}
		}
		
		while(left<=right){
			if(target==nums[right]){
				result[1]=right;
				break;
			}else{
				right--;
			}
		}
		
		return result;
	}

}
