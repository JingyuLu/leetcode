package lwh023hard.merge_k_sorted_list;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class MergeKSortedList {

	public static void main(String[] args) {
		MergeKSortedList mksl=new MergeKSortedList();
		
		ListNode[] lists = null;
		
		ListNode ln=mksl.mergeKLists(lists);
	}
	//heap sort
	public ListNode mergeKLists(ListNode[] lists) {
		
		if(lists==null||lists.length==0) {
			return null;
		}
		Comparator<ListNode> comp=new Comparator<ListNode>() {
			
			@Override
			public int compare(ListNode o1, ListNode o2) {
				// TODO Auto-generated method stub
				return o1.val-o2.val;
			}
		};
		Queue<ListNode> heap=new PriorityQueue<ListNode>(lists.length,comp);

		for(int i=0;i<lists.length;i++) {
			if(lists[i]!=null) {	
				heap.offer(lists[i]);
			}
		}
		ListNode head=new ListNode(-1);
		ListNode p=head;
		while(!heap.isEmpty()) {
			ListNode node=heap.poll();	
			p.next=node;
			p=node;	
			if(node.next!=null) {
				heap.add(node.next);
			}
		}
		return head.next;

	}

}

class ListNode {
	int val;
	ListNode next;

	ListNode(int x) {
		val = x;
	}
}