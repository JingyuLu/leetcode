package lwh017medium.letter_combinations_of_a_phone_number;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class LetterCombinationsOfPhoneNumber {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		LetterCombinationsOfPhoneNumber lcopn=new LetterCombinationsOfPhoneNumber();
		Scanner sc=new Scanner(System.in);
		String inputStr=sc.nextLine();
		List<String> res=lcopn.letterCombination(inputStr);
		System.out.println(res);
	}

	/**
	 * @param inputStr
	 * @return
	 */
	public List<String> letterCombination(String digits) {
		List<String> res=new ArrayList<String>();
		if(digits==null||digits.length()==0){	
			return res;
		}
		String[] map={"", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
		int[] phoneNum={0,1,2,3,4,5,6,7,8,9};	
		StringBuilder sb=new StringBuilder();
		helper(res,map,0,sb,digits);
		
		return res;
	}

	/**
	 * @param res
	 * @param map
	 * @param index
	 * @param i
	 * @param digits
	 */
	private void helper(List<String> res, String[] map, int index, StringBuilder sb, String digits) {
		if(index==digits.length()){
			//find a result,add to list
			if(sb.length()!=0){
				res.add(sb.toString());
			}
		}else{
			
			String candidates=map[digits.charAt(index)-'0'];	
			
			for(int i=0;i<candidates.length();i++){
				sb.append(candidates.charAt(i));
				helper(res,map,index+1,sb,digits);
				sb.deleteCharAt(sb.length()-1);
			}
		}
	}
}
