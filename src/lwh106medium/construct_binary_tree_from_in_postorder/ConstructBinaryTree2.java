package lwh106medium.construct_binary_tree_from_in_postorder;

import java.util.HashMap;

public class ConstructBinaryTree2 {

	public static void main(String[] args) {
		ConstructBinaryTree2 cbt2 = new ConstructBinaryTree2();
		int[] inorder = { 9, 3, 15, 20, 7 };
		int[] postorder = { 9, 15, 7, 20, 3 };

		TreeNode result = cbt2.buildTree(inorder, postorder);
		System.out.println(postorder[postorder.length-1]);
	}

	public TreeNode buildTree(int[] inorder, int[] postorder) {
		if (postorder == null || postorder.length == 0 || inorder == null || inorder.length == 0) {
			return null;
		}
		if (postorder.length != inorder.length) {
			return null;
		}
		
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
	    for(int i=0;i<inorder.length;i++)
	    {
	        map.put(inorder[i],i);
	    }
		return builderHelper(inorder, 0, inorder.length - 1, postorder, 0, postorder.length - 1);
	}

	public TreeNode builderHelper(int[] inorder, int inStart, int inEnd, int[] postorder, int pStart, int pEnd) {

		if (inStart > inEnd || pStart > pEnd) {
			return null;
		}
		TreeNode root = new TreeNode(postorder[pEnd]);
		int curPos = 0;
		for (int i = inStart; i <= inEnd; i++) {
			if (postorder[pEnd] == inorder[i]) {
				curPos = i;
				break;
			}
		}
		
		root.left = builderHelper(inorder, inStart, curPos - 1, postorder, pStart, pEnd + curPos - inStart - 1);
		root.right = builderHelper(inorder, curPos + 1, inEnd, postorder, pEnd - (inEnd - curPos), pEnd - 1);
		return root;
	}

}

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}