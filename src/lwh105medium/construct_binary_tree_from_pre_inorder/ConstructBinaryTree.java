package lwh105medium.construct_binary_tree_from_pre_inorder;

public class ConstructBinaryTree {

	public static void main(String[] args) {
		ConstructBinaryTree cbt = new ConstructBinaryTree();
		int[] preorder = { 3, 9, 20, 15, 7 };
		int[] inorder = { 9, 3, 15, 20, 7 };

		TreeNode result = cbt.buildTree(preorder, inorder);

	}

	public TreeNode buildTree(int[] preorder, int[] inorder) {
		if (preorder == null || preorder.length == 0 || inorder == null || inorder.length == 0) {
			return null;
		}
		if (preorder.length != inorder.length) {
			return null;
		}
		return builderHelper(preorder, 0, inorder, 0, inorder.length - 1);
	}

	public TreeNode builderHelper(int[] preorder, int preIndex, int[] inorder, int start, int end) {

		if (start > end) {
			return null;
		}
		TreeNode root = new TreeNode(preorder[preIndex]);
		int curPos = 0;

		for (int i = start; i <= end; i++) {
			if(preorder[preIndex]==inorder[i]) {
				curPos=i;
				break;	
			}
		}
		root.left=builderHelper(preorder, preIndex+1, inorder, start, curPos-1);	

		root.right=builderHelper(preorder, preIndex+curPos-start+1, inorder, curPos+1, end);

		return root;
	}

}

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}
