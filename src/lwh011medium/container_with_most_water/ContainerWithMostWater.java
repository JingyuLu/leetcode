package lwh011medium.container_with_most_water;

/*
Given n non-negative integers a1, a2, ..., an, where each represents a point at 
coordinate (i, ai). n vertical lines are drawn such that the two endpoints of 
line i is at (i, ai) and (i, 0). Find two lines, which together with x-axis 
forms a container, such that the container contains the most water.

Note: You may not slant the container and n is at least 2
 */
/*
给定n个非负整数a1,a2,...,an，其中每个代表一个点坐标（i,ai）。

n个垂直线段例如线段的两个端点在（i,ai）和（i,0）。

找到两个线段，与x轴形成一个容器，使其包含最多的水。

备注：你不必倾倒容器。
 */
public class ContainerWithMostWater {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		ContainerWithMostWater cwmw=new ContainerWithMostWater();
		
		int[] array={1,2,1};
		int result=cwmw.maxArea(array);
	}

	/**
	 * @param array
	 * @return
	 */
	private int maxArea(int[] height) {
		int left=0;
		int right=height.length-1;
		int max=0;

		while(left<right){
			max=Math.max(max, Math.min(height[left], height[right])*(right-left));
//			System.out.println(max+" "+left+" "+right);
			if(height[left]<height[right]){
				left++;
			}else{
				right--;
			}
		}
		return max;
	}

}
