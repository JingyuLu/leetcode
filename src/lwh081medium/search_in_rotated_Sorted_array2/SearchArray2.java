package lwh081medium.search_in_rotated_Sorted_array2;

import java.util.Arrays;

public class SearchArray2 {

	public static void main(String[] args) {
		
		SearchArray2 sa2=new SearchArray2();
		int[] nums= {2,5,6,0,0,1,2};
		int target=0;
		
		boolean result=sa2.search(nums,target);
	}

	public boolean search(int[] nums, int target) {
		
		if(nums.length==0) {
			return false;
		}
		Arrays.sort(nums);
		int left=0;
		int right=nums.length-1;
		while(left<=right) {
			int mid=(left+right)/2;
			if(nums[mid]==target) {
				return true;
			}else if(nums[mid]<target) {
				left=mid+1;
			}else {
				right=mid-1;
			}
		}
		return false;
	}


}
