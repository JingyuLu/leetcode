package lwh118easy.pascal_triangle;

import java.util.ArrayList;
import java.util.List;

public class PascalTriangle {

	public static void main(String[] args) {
		PascalTriangle pt=new PascalTriangle();
		int numRows=5;
		
		List<List<Integer>> result=pt.generate(numRows);
	}

	private List<List<Integer>> generate(int numRows) {
		
		if(numRows<0) {
			return null;
		}
		List<List<Integer>> res=new ArrayList<List<Integer>>();
		for(int i=0;i<numRows;i++) {
			List<Integer> eachRow=new ArrayList<Integer>();
			eachRow.add(0,1);
			for(int j=1;j<i;j++) {
				if(i-1>=0) {
					List<Integer> last=res.get(i-1);
					int num=last.get(j-1)+last.get(j);
					eachRow.add(j,num);
				}
			}
			if(i>0) {
				eachRow.add(i,1);
			}
			res.add(eachRow);
		}
		return res;
	}

}
