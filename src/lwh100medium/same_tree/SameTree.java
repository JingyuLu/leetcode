package lwh100medium.same_tree;

public class SameTree {

	public static void main(String[] args) {

		SameTree st = new SameTree();
		TreeNode p = new TreeNode(1);
		TreeNode q = new TreeNode(2);

		boolean result = st.isSameTree(p, q);
	}

	public boolean isSameTree(TreeNode p, TreeNode q) {

		if (p == null && q == null) {
			return true;
		}
		if (p == null || q == null) {
			return false;
		}
		if (p.val == q.val) {
			return isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
		} else {
			return false;
		}
	}

}

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	public TreeNode(int x) {
		val = x;
	}
}