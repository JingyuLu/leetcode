ackage lwh070easy.climbing_stairs;

import java.util.Scanner;

public class ClimbingStairs {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		ClimbingStairs cs=new ClimbingStairs();
		Scanner sc=new Scanner(System.in);
		int n=sc.nextInt();
		sc.nextLine();
		
		int i=cs.climbStairs(n);
	}

	/**
	 * @param n
	 * @return
	 */
	private int climbStairs(int n) {
		
//		//solution 1 over time
//		if (n == 0)
//	        return 1;
//	    else if (n < 0)
//	        return 0;
//	    return climbStairs(n - 1) + climbStairs(n - 2);
		
		//solution2
		if(n==0||n==1||n==2)
            return n;
        int [] dp = new int[n+1];
        dp[0]=0;
        dp[1]=1;
        dp[2]=2;
        
        for(int i = 3; i<n+1;i++){
            dp[i] = dp[i-1]+dp[i-2];
        }
        return dp[n];
	}


}
