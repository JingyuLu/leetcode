package lwh039medium.combination_sum;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CombinationSum {

	List<List<Integer>> list;

	public static void main(String[] args) {
		CombinationSum cs = new CombinationSum();

		int[] nums = { 2, 3, 6, 7 };
		int target = 7;

		List<List<Integer>> result = cs.combinationSum(nums, target);

	}

	private List<List<Integer>> combinationSum(int[] candidates, int target) {
		list = new ArrayList<List<Integer>>();
		List<Integer> temp = new ArrayList<Integer>();
		Arrays.sort(candidates);
		helper(temp, candidates, target, 0);
		return list;
	}

	private void helper(List<Integer> temp, int[] candidates, int target, int index) {
		if (target < 0) {

			return;
		} else if (target == 0) {

			List<Integer> oneComb = new ArrayList<Integer>(temp);
			list.add(oneComb);
		} else {
			for (int i = index; i < candidates.length; i++) {
				temp.add(candidates[i]);
				helper(temp, candidates, target - candidates[i], i);
				temp.remove(temp.size() - 1);
			}
		}
	}

}
