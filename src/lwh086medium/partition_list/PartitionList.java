package lwh086medium.partition_list;

import javax.print.DocFlavor.INPUT_STREAM;

public class PartitionList {

	public static void main(String[] args) {
		
		PartitionList pl=new PartitionList();
		ListNode head=new ListNode(0);
		int x=3;
		
		ListNode result=pl.partition(head,x);
	}

	public ListNode partition(ListNode head, int x) {
		
		ListNode preHead=new ListNode(0);
		preHead.next=head;
		ListNode pre=preHead;
		ListNode cur=head;
		ListNode insertPos=null;
		
		while(cur!=null) {
			//locate insert position
			if(cur.val>=x&&insertPos==null) {
				insertPos=pre;
			}
			//insert elements
			if(cur.val<x&&insertPos!=null) {
				pre.next=pre.next.next;	
				cur.next=insertPos.next;	
				insertPos.next=cur;	
				insertPos=insertPos.next;
				cur=pre.next;
				continue;
			}
			pre=pre.next;
			cur=cur.next;
		}
		
		return preHead.next;
	}

}

class ListNode {
	int val;
	ListNode next;

	ListNode(int x) {
		val = x;
	}
}
