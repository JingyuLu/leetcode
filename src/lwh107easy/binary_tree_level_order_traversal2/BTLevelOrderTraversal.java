package lwh107easy.binary_tree_level_order_traversal2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BTLevelOrderTraversal {

	List<List<Integer>> res = new ArrayList<>();

	public static void main(String[] args) {
		BTLevelOrderTraversal lot = new BTLevelOrderTraversal();
		TreeNode root = new TreeNode(3);

		List<List<Integer>> result = lot.levelOrderBottom(root);
	}

	public List<List<Integer>> levelOrderBottom(TreeNode root) {

		if (root == null) {
			return res;
		}
		downLevelOrderTraversal(root, 0);

		Collections.reverse(res);
		return res;
	}

	public void downLevelOrderTraversal(TreeNode root, int level) {
		if (root == null) {
			return;
		}
		if (res.size() == level) {
			// meaning this level list is empty, need to add new list
			List<Integer> temp = new ArrayList<>();
			temp.add(root.val);
			res.add(level, temp);
		} else {
			res.get(level).add(root.val);
		}
		downLevelOrderTraversal(root.left, level + 1);
		downLevelOrderTraversal(root.right, level + 1);
	}

}

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}