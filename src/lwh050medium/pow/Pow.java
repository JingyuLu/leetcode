package lwh050medium.pow;

public class Pow {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Pow p=new Pow();
		
		double x=2.1;
		int n=3;
		
		double result=p.myPow(x,n);
	}

	private double myPow(double x, int n) {
		
		if(x==0&&n>0) {
			return 0;
		}
		
		if(n<0) {			
			return 1/x*myPow(1/x, -(n+1));// pass
		}else if(n==0) {
			return 1;
		}else if(n==2) {
			return x*x;
		}else if(n%2==0) {
			return myPow(myPow(x, n/2), 2);
		}else {
			return x*myPow(myPow(x, n/2), 2);
		}
	}

}
