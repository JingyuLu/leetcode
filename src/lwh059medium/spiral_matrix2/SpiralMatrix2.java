package lwh059medium.spiral_matrix2;

import java.util.Stack;

public class SpiralMatrix2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SpiralMatrix2 sm2=new SpiralMatrix2();
		
		int n=3;
		
		int[][] result=generateMatrix(n);
	}

	private static int[][] generateMatrix(int n) {
		
		int[][] array=new int[n][n];
		int counter=1;
		
		int top=0;
		int bottom=n-1;
		int left=0;
		int right =n-1;
		
//		int counter=n*n;
//		Stack<Integer> nums=new Stack<>();
//		for(int index=0;index<counter;index++) {
//			nums.push(counter);
//			counter--;
//		}
		
		while (left <= right && top <= bottom) {
			for (int col = left; col <= right; col++) {
				array[top][col] = counter++;
			}
			top++;

			for (int row = top; row <= bottom; row++) {
				array[row][right] = counter++;
			}
			right--;

			for (int col = right; col >= left; col--) {
				array[bottom][col] = counter++;
			}
			bottom--;

			for (int row = bottom; row >= top; row--) {
				array[row][left] = counter++;
			}
			left++;

		}
		
		return array;
	}

}
