package lwh052hard.n_queens2;

public class NQueens2 {

	int count=0;
	
	public static void main(String[] args) {

		NQueens2 nq2=new NQueens2();
		int n=4;
		
		int result=nq2.totalNQueens(n);
	}

	public int totalNQueens(int n) {
		
		boolean[] column=new boolean[n];
		boolean[] topLeft=new boolean[2*n-1];
		boolean[] botLeft=new boolean[2*n-1];
		
		isValid(0,column,topLeft,botLeft,n);
		
		return count;
	}

	private void isValid(int row, boolean[] column, boolean[] topLeft, boolean[] botLeft, int n) {
		if(row==n) {
			count++;
			return;
		}
		
		for(int i=0;i<n;i++) {
			int x1=row+i;
			int x2=row-i+n-1;
			if(column[i]||topLeft[x1]||botLeft[x2]) {
				continue;
			}
			column[i]=true;
			topLeft[x1]=true;
			botLeft[x2]=true;
			isValid(row+1, column, topLeft, botLeft, n);
			column[i]=false;
			topLeft[x1]=false;
			botLeft[x2]=false;
		}
	}

}
