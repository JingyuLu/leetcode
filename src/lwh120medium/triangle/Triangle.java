package lwh120medium.triangle;

import java.util.ArrayList;
import java.util.List;

import lwh012medium.integer_to_roman.IntegerToRoman;

public class Triangle {

	public static void main(String[] args) {
		Triangle t=new Triangle();
		List<List<Integer>> triangle=new ArrayList<>(new ArrayList<>(2));
		
		int result=t.minimumTotal(triangle);
	}

	private int minimumTotal(List<List<Integer>> triangle) {
		
		int size=triangle.size();
		Integer[] dp=triangle.get(size-1).toArray(new Integer[size]);
		for(int level=size-1;level>0;level--) {
			for(int i=0;i<level;i++) {
				dp[i]=Math.min(dp[i], dp[i+1])+triangle.get(level-1).get(i);
			}
		}
		
		return dp[0];
	}

}
