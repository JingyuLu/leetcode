package lwh120medium.triangle;

import java.util.ArrayList;
import java.util.List;

public class MyTriangle {

	public static void main(String[] args) {
		
		MyTriangle t=new MyTriangle();
		List<List<Integer>> triangle=new ArrayList<>(new ArrayList<>(2));
		
		int result=t.minimumTotal(triangle);
	}

	/*
	 * [2],
      [3,4],
     [6,5,7],
    [4,1,8,3]
    
    triangle.size()=4
    triangle.get(0)=[2]-��Ȼ��list
	 */
	public int minimumTotal(List<List<Integer>> triangle) {
		//wrong,Each step you may move to adjacent numbers on the row below.
		
		if(triangle==null) {
			return 0;
		}
		int res=triangle.get(0).get(0);
		if(triangle.size()==1) {
			return res;
		}
		int size=triangle.size();
		List<Integer> list=new ArrayList<>();
		int temp=0;
		
		for(int i=1;i<size;i++) {
			list=triangle.get(i);
			sortList(list);
			temp=list.get(0);
			res+=temp;
		}
		
		return res;
	}

	public void sortList(List<Integer> list) {
		// TODO Auto-generated method stub
		list.sort(null);
	}

}
