package lwh042hard.trapping_rain_water;

public class TrappingRainWater {

	public static void main(String[] args) {

		TrappingRainWater trw=new TrappingRainWater();
		int[] height= {0,1,0,2,1,0,1,3,2,1,2,1};
		
		int res=trw.trap(height);
	}

	private int trap(int[] height) {
		
		int len=height.length;
		if(len==0) {
			return 0;
		}
		
		int left=0;
		int right=len-1;
		int leftMost=height[left];
		int rightMost=height[right];
		int res=0;
		
		while(left<right) {
			if(leftMost<rightMost) {
				res+=leftMost-height[left++];
				leftMost=Math.max(leftMost, height[left]);
			}else {
				res+=rightMost-height[right--];
				rightMost=Math.max(rightMost, height[right]);
			}
		}
		
		return res;
	}

}
