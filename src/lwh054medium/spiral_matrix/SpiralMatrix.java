package lwh054medium.spiral_matrix;

import java.util.ArrayList;
import java.util.List;

public class SpiralMatrix {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SpiralMatrix sm=new SpiralMatrix();
		
		int[][]	matrix= {
		 { 1, 2, 3},
		 { 4, 5, 6},
		 { 7, 8, 9}
		};
		
		List<Integer> list=spiralOrder(matrix);
	}

	private static List<Integer> spiralOrder(int[][] matrix) {
		
		List<Integer> result=new ArrayList<>();
		
		if(matrix==null||matrix.length==0) {
			return result;
		}
		
		int row=matrix.length;
		int col=matrix[0].length;
		int left=0;
		int right=col-1;
		int top=0;
		int bottom=row-1;
		
		while(result.size()<row*col) {
			for(int tempCol=left;tempCol<=right;tempCol++) {
				result.add(matrix[top][tempCol]);
			}
			top++;
			if(result.size()<row*col) {
				for(int tempRow=top;tempRow<=bottom;tempRow++) {
					result.add(matrix[tempRow][right]);
				}
				right--;
			}
			if(result.size()<row*col) {
				for(int tempCol=right;tempCol>=left;tempCol--) {
					result.add(matrix[bottom][tempCol]);
				}
				bottom--;
			}
			if(result.size()<row*col) {
				for(int tempRow=bottom;tempRow>=top;tempRow--) {
					result.add(matrix[tempRow][left]);
				}
				left++;
			}
		}
		
		return result;
	}

}
