package lwh109medium.convert_sorted_list_to_bst;

public class ListToBST {

	ListNode curHead=null;
	public static void main(String[] args) {
		ListToBST ltb=new ListToBST();
		ListNode head=new ListNode(-10);
		
		TreeNode result=ltb.sortedListToBST(head);
	}
	public TreeNode sortedListToBST(ListNode head) {
		
		if(head==null) {
			return null;
		}
		curHead=head;
		int length=0;
		while(head!=null) {
			length++;
			head=head.next;
		}
		return buildBST(0, length-1);
	}

	public TreeNode buildBST(int start, int end) {
		
		if(start>end) {
			return null;
		}
//		int mid=start+(end-start)/2;
		int mid=(start+end)/2;
		TreeNode left=buildBST(start, mid-1);
		TreeNode root=new TreeNode(curHead.val);
		root.left=left;
		curHead=curHead.next;
		root.right=buildBST(mid+1, end);
		
		return root;

	}

}

class ListNode {
	int val;
	ListNode next;

	ListNode(int x) {
		val = x;
	}
}

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}