package lwh096medium.unique_binary_search_tree;

public class UniqueBinarySearchTree {

	public static void main(String[] args) {
		UniqueBinarySearchTree ubst=new UniqueBinarySearchTree();
		int n=6;
		
		int result=ubst.numTrees(n);
	
	}

	private int numTrees(int n) {
		
		int[] array=new int[n+1];
		array[0]=1;
		array[1]=1;
		for(int i=2;i<=n;i++) {
			for(int j=0;j<i;j++) {
				array[i]=array[i]+array[j]*array[i-j-1];
			}
		}
		
		return array[n];
	}

}
