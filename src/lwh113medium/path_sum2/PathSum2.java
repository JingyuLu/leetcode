package lwh113medium.path_sum2;

import java.util.ArrayList;
import java.util.List;

public class PathSum2 {

	public static void main(String[] args) {
		PathSum2 ps2 = new PathSum2();
		TreeNode root = new TreeNode(3);
		int sum = 22;

		List<List<Integer>> list = ps2.pathSum(root, sum);
	}

	public List<List<Integer>> pathSum(TreeNode root, int sum) {

		List<List<Integer>> res = new ArrayList<List<Integer>>();
		if (root == null) {
			return res;
		}
		List<Integer> temp = new ArrayList<Integer>();
		mathHelper(root, sum, res, temp);

		return res;
	}

	public void mathHelper(TreeNode root, int sum, List<List<Integer>> res, List<Integer> temp) {

		temp.add(root.val);
		sum = sum - root.val;

		if (root.left == null && root.right == null) {
			if (sum == 0) {
				res.add(new ArrayList<>(temp));
			}
		}
		if (root.left != null) {
			mathHelper(root.left, sum, res, temp);
		}
		if (root.right != null) {
			mathHelper(root.right, sum, res, temp);
		}
		temp.remove(temp.size() - 1);

	}

}

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}