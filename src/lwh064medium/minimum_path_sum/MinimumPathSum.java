package lwh064medium.minimum_path_sum;

public class MinimumPathSum {

	public static void main(String[] args) {
		
		MinimumPathSum mps=new MinimumPathSum();
		
		int[][] grid= {
				{1,3,1},
				{1,5,1},
				{4,2,1}
		};
		
		int result=mps.minPathSum(grid);
	}

	private int minPathSum(int[][] grid) {
		
		int rowLength=grid.length;
		int colLength=grid[0].length;
		
		for(int i=1;i<rowLength;i++) {
			grid[i][0]=grid[i][0]+grid[i-1][0];
		}
		for(int j=1;j<colLength;j++) {
			grid[0][j]=grid[0][j]+grid[0][j-1];
		}
		
		for(int i=1;i<rowLength;i++) {
			for(int j=1;j<colLength;j++) {
				grid[i][j]=grid[i][j]+Math.min(grid[i-1][j], grid[i][j-1]);
			}
		}
		
		return grid[rowLength-1][colLength-1];
	}

}
