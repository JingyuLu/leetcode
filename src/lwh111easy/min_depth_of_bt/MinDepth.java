package lwh111easy.min_depth_of_bt;

public class MinDepth {

	public static void main(String[] args) {
		MinDepth md = new MinDepth();
		TreeNode root=new TreeNode(3);
		
		int result=md.minDepth(root);
	}

	public int minDepth(TreeNode root) {
		
		if(root==null) {
			return 0;
		}
		if(root!=null&&root.left==null&&root.right==null) {
			return 1;
		}
		int leftDepth=minDepth(root.left);
		int rightDepth=minDepth(root.right);
		
		if(leftDepth==0||rightDepth==0) {
			return (leftDepth>=rightDepth?leftDepth:rightDepth)+1;
		}else {
			return Math.min(leftDepth, rightDepth)+1;
		}
		
	}

}

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}