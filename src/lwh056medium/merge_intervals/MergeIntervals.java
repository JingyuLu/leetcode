package lwh056medium.merge_intervals;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MergeIntervals {

	public static void main(String[] args) {
		
		MergeIntervals mi=new MergeIntervals();
		
		List<Interval> intervals = null;
		
		List<Interval> result=mi.merge(intervals);
	}

	private List<Interval> merge(List<Interval> intervals) {
		
		if(intervals==null||intervals.size()<=1) {
			return intervals;
		}
		Collections.sort(intervals, new Comparator<Interval>() {
			@Override
			public int compare(Interval o1, Interval o2) {
				// TODO Auto-generated method stub
				return o1.start-o2.start;
			}
		});
		
		Interval pre=intervals.get(0);
		for(int i=1;i<intervals.size();i++) {
			Interval cur=intervals.get(i);
			if(pre.end>=cur.start) {
				pre.end=Math.max(pre.end, cur.end);
				intervals.remove(cur);
				i--;
			}else {
				pre=cur;
			}
		}
		
		return intervals;
	}

}

class Interval{
	int start;
	int end;
	Interval(){
		start=0;
		end=0;
	}
	Interval(int s,int e){
		start=s;
		end=e;
	}
}
