package lwh083easy.remove_duplicates_from_sorted_list;

public class RemoveDuplicatesList {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		RemoveDuplicatesList rdl=new RemoveDuplicatesList();
		
//		ListNode head=rdl.new ListNode(1);// if ListNode is inner class
		ListNode head=new ListNode(1);
		
		ListNode newHead=rdl.removeDuplicatesList(head);
		
	}

	/**
	 * @param head
	 * @return 
	 */
	private ListNode removeDuplicatesList(ListNode head) {

//		//solution 1 over time
//		if(head==null){
//			return head;
//		}
//		
//		ListNode pre=head;
//		ListNode cur=head.next;
//		while(cur!=null){
//			if(cur.val==pre.val){
//				pre.next=cur.next;
//			}else{
//				pre=cur;
//				cur=cur.next;
//			}
//		}
		
		//solution 2
		ListNode cur=head;
		while(cur!=null&&cur.next!=null){
			if(cur.next.val==cur.val){
				cur.next=cur.next.next;
			}else{
				cur=cur.next;
			}
		}
		return head;
	}

}

class ListNode{
	int val;
	ListNode next;
	
	public ListNode(int x) {
		val=x;
	}
}