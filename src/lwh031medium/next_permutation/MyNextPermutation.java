package lwh031medium.next_permutation;

import java.util.Arrays;

public class MyNextPermutation {

	public static void main(String[] args) {
		MyNextPermutation np = new MyNextPermutation();

		int[] nums = { 1, 2, 3 };

		np.nextPermutation(nums);
	}

	private void nextPermutation(int[] nums) {

		int flag = 0;
		int len = nums.length;
		if (len == 0) {
			return;
		}

		for (int i = len - 2; i >= 0; i--) {
			int tmp = nums[i];
			for (int j = len - 1; j > i; j--) {

				if (tmp < nums[j]) {
					nums[i] = nums[j];
					nums[j] = tmp;
					flag = 1;
					break;
				}
			}
			if (flag == 1) {

				Arrays.sort(nums, i + 1, len);
				break;
			}
		}
		
		if (flag == 0) {
			Arrays.sort(nums);
		}
	}

}
