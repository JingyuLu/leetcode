package lwh031medium.next_permutation;

import java.util.Arrays;

public class NextPermutation {

	public static void main(String[] args) {
		NextPermutation np=new NextPermutation();
		
		int[] nums={1,2,3};
		
		np.nextPermutation(nums);
	}

	private void nextPermutation(int[] nums) {
		if(nums==null||nums.length==0){
			return;
		}
		int length=nums.length;
		for(int i=length-1;i>=0;i--){

			if(i-1>=0&&nums[i-1]<nums[i]){
				int k=i;
				for(int j=i+1;j<length;j++){

					if(nums[i-1]<nums[j]){
						k=j;
					}
				}
				int temp=nums[k];
				nums[k]=nums[i-1];
				nums[i-1]=temp;
				Arrays.sort(nums,i,length);
				return;
			}
		}

		for(int i=0;i<length/2;i++){
			int temp=nums[i];
			nums[i]=nums[length-1-i];
			nums[length-1-i]=temp;
		}
		return;
	}

}
