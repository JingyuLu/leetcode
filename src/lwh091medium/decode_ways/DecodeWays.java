package lwh091medium.decode_ways;

public class DecodeWays {

	public static void main(String[] args) {
		
		DecodeWays dw=new DecodeWays();
		String s="226";
		
		int result=dw.numDecodings(s);
	}

	public int numDecodings(String s) {
		
		if(s==null||s.length()==0) {
			return 0;
		}
		char[] sArray=s.toCharArray();
		int[] nums=new int[sArray.length+1];
		nums[0]=1;
		for(int i=1;i<=sArray.length;i++) {
			if(sArray[i-1]!='0') {
				nums[i]=nums[i]+nums[i-1];
			}
			if(i>1&&sArray[i-2]=='1') {
				nums[i]=nums[i]+nums[i-2];
			}
			if(i>1&&sArray[i-2]=='2'&&sArray[i-1]>='0'&&sArray[i-1]<='6') {
				nums[i]=nums[i]+nums[i-2];
			}
		}
		
		return nums[sArray.length];
	}

}
