package lwh013easy.roman_to_integer;

import java.util.HashMap;
import java.util.Scanner;

/*
 * 罗马数字转换为int数
Given a roman numeral, convert it to an integer.

Input is guaranteed to be within the range from 1 to 3999
 */
public class RomanToInteger {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		RomanToInteger rti=new RomanToInteger();
		String inputRoman="LIV";
		
		int resultNum=rti.romanToInteger(inputRoman);
		System.out.println(resultNum);
	}

	/**
	 * @param inputRoman
	 * @return
	 */
	private int romanToInteger(String inputRoman) {

		HashMap<Character, Integer> map=new HashMap<Character, Integer>();
		map.put('I', 1);
		map.put('V', 5);
		map.put('X', 10);
		map.put('L', 50);
		map.put('C', 100);
		map.put('D', 500);
		map.put('M', 1000);
		
//		//solution 1
//		int value=map.get(inputRoman.charAt(0));
//		for(int i=1;i<inputRoman.length();i++){
//			if(map.get(inputRoman.charAt(i))>map.get(inputRoman.charAt(i-1))){
////				value=value+map.get(inputRoman.charAt(i))-2*map.get(inputRoman.charAt(i-1));
//				value=map.get(inputRoman.charAt(i))-map.get(inputRoman.charAt(i-1));
//			}else{
//				value=value+map.get(inputRoman.charAt(i));
//			}
//		}	//	end solution 1
		
		//solution 2
		int length=inputRoman.length();
		int result=0;
		int preValue=0;
		//从右向左计算
		for(int i=length-1;i>=0;i--){
			char key=inputRoman.charAt(i);
			int value=map.get(key);
			if(value>=preValue){
				result+=value;
			}else{
				result-=value;
			}
			
			preValue=value;
		}
		return result;
	}

}
