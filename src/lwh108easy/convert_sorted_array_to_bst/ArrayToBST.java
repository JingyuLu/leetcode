package lwh108easy.convert_sorted_array_to_bst;

public class ArrayToBST {

	public static void main(String[] args) {
		ArrayToBST atb=new ArrayToBST();
		int[] nums= {-10,-3,0,5,9};
//		int length=nums.length;
		
		TreeNode result=atb.sortedArrayToBST(nums);
	}

	public TreeNode sortedArrayToBST(int[] nums) {
		int left=0;
		int right=nums.length-1;
		TreeNode node=creatTreeNode(nums,left,right);
		
		return node;
	}

	public TreeNode creatTreeNode(int[] nums, int left, int right) {
		if(left>right) {
			return null;
		}

		int mid=(left+right)/2;
		TreeNode node=new TreeNode(nums[mid]);
		node.left=creatTreeNode(nums, left, mid-1);
		node.right=creatTreeNode(nums, mid+1, right);
		
		return node;
	}

}

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}