package lwh036medium.valid_sudoku;

import java.util.HashMap;
import java.util.Map;

public class ValidSudoku {

	public static void main(String[] args) {
		ValidSudoku vs=new ValidSudoku();
		
		char[][] board={
				{'5','3','.','.','7','.','.','.','.'},
				{'6','.','.','1','9','5','.','.','.'},
				{'.','9','8','.','.','.','.','6','.'},
				{'8','.','.','.','6','.','.','.','3'},
				{'4','.','.','8','.','3','.','.','.'},
				{'7','.','.','.','2','.','.','.','6'},
				{'.','6','.','.','.','.','2','8','.'},
				{'.','.','.','4','1','9','.','.','5'},
				{'.','.','.','.','8','.','.','7','9'}
		};
		
		boolean result=vs.validSudoku(board);
	}

	private boolean validSudoku(char[][] board) {
		
		Map<Character, Integer> map=new HashMap<Character, Integer>();
		//row
		for(int row=0;row<board.length;row++){
			for(int col=0;col<board[0].length;col++){
				if(board[row][col]=='.'){
					continue;
				}else{
					if(map.containsKey(board[row][col])){
						return false;
					}else{
						map.put(board[row][col], 1);
					}
				}			
			}
			map.clear();
		}
		
		//column
		for(int col=0;col<board[0].length;col++){
			for(int row=0;row<board.length;row++){
				if(board[row][col]=='.'){
					continue;
				}else{
					if(map.containsKey(board[row][col])){
						return false;
					}else{
						map.put(board[row][col], 1);
					}
				}
			}
			map.clear();
		}
		
		//3*3
		for(int row=0;row<board.length;row+=3){
			for(int col=0;col<board[0].length;col+=3){
				for(int flag=0;flag<9;flag++){
					if(board[row+flag/3][col+flag%3]=='.'){	
						continue;
					}else{
						if(map.containsKey(board[row+flag/3][col+flag%3])){
							return false;
						}else{
							map.put(board[row+flag/3][col+flag%3], 1);
						}
					}
					map.clear();	//do not forget to clear map every when finish each validation
				}
			}
		}
		return true;
	}

}
