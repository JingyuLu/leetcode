package lwh002medium.add_two_numbers;

public class MyAddTwoNum {

	public static void main(String[] args) {
		
		MyAddTwoNum matn=new MyAddTwoNum();
		MyListNode l1=new MyListNode(1);
		MyListNode l2=new MyListNode(2);
		
		MyListNode result=matn.addTwoNumbers(l1,l2);
	}

	public MyListNode addTwoNumbers(MyListNode l1, MyListNode l2) {
		
		if(l1==null) {
			return l2;
		}
		if(l2==null) {
			return l1;
		}
		MyListNode resNode=new MyListNode(-1);
		MyListNode l3=resNode;
		int sum=0;
		
		while(l1!=null||l2!=null) {
			
			if(l1!=null) {
				sum+=l1.val;
				l1=l1.next;
			}
			if(l2!=null) {
				sum+=l2.val;
				l2=l2.next;
			}
			l3.next=new MyListNode(sum%10);
			sum=sum/10;//判断是否进位0/1
			l3=l3.next;
		}
		
		//为了防止l1,l2只有一个数且进位[5][5]
		if(sum==1) {
			l3.next=new MyListNode(1);
		}
		return resNode.next;
	}

}
class MyListNode {
	      int val;
	      MyListNode next;

	MyListNode(int x) {
		val = x;
	}
}