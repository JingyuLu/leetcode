package lwh002medium.add_two_numbers;

import java.awt.List;

/*
You are given two non-empty linked lists representing two non-negative integers. 
The digits are stored in reverse order and each of their nodes contain a single digit. 
Add the two numbers and return it as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.

Example

Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
Output: 7 -> 0 -> 8
Explanation: 342 + 465 = 807.
 */
public class AddTwoNumbers {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		AddTwoNumbers atn=new AddTwoNumbers();
		
		ListNode l1=new ListNode(1);
		ListNode l2=new ListNode(2);
		System.out.println(6%10);
//		ListNode lRe=atn.addTwoNumbers(l1,l2);
	}

	/**
	 * @param l1
	 * @param l2
	 * @return
	 */
	private ListNode addTwoNumbers(ListNode l1, ListNode l2) {
		
		if(l1==null){
			return l2;
		}
		if(l2==null){
			return l1;
		}
		
		int carry=0;
		ListNode sum=new ListNode(-1);
		ListNode l3 = sum;
		
		while(l1!=null||l2!=null){

			if(l1!=null){
				carry=carry+l1.val;
				l1=l1.next;
			}
			if(l2!=null){
				carry=carry+l2.val;
				l2=l2.next;
			}
			l3.next=new ListNode(carry%10);
			carry=carry/10;
			l3=l3.next;
		}
		if(carry==1){
			l3.next=new ListNode(1);
		}
		
		return sum.next;
	}

}

class ListNode{
	int val;
	ListNode next;
	ListNode(int x) { 
		val = x; 
	}
}
