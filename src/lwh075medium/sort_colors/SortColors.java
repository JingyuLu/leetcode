package lwh075medium.sort_colors;


public class SortColors {

	public static void main(String[] args) {
		
		SortColors sc=new SortColors();
		
		int[] nums= {2,0,2,1,1,0};
		
		sc.sortColors(nums);
	}

	public void sortColors(int[] nums) {
		
		int flag=1;
		int i=0;
		int j=nums.length-1;
		int k=0;
		
		while(k<=j) {
			if(nums[k]<flag) {
				exchange(nums,i,k);
				i++;
				k++;
			}else if(nums[k]>flag) {
				exchange(nums, j, k);
				j--;
//				k;	//because we should make sure the new number moved to nums[k]
			}else {
				k++;
			}
		}
	}

	public void exchange(int[] nums, int i, int k) {
		// TODO Auto-generated method stub
		int temp=nums[i];
		nums[i]=nums[k];
		nums[k]=temp;
	}

}
