package lwh016medium.three_sum_closest;

import java.util.Arrays;

/*
Given an array S of n integers, find three integers in S such that the sum is closest to a given number, target. Return the sum of the three integers. You may assume that each input would have exactly one solution.

    For example, given array S = {-1 2 1 -4}, and target = 1.

    The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).
 */
public class MyThreeSumClosest {

	public static void main(String[] args) {
		MyThreeSumClosest tsc=new MyThreeSumClosest();
		
		int[] inputArray={-55,-24,-18,-11,-7,-3,4,5,6,9,11,23,33};
		int target=0;
		
		int result=tsc.threeSumClosest(inputArray,target);
		System.out.println(result);
	}

	/**
	 * @param inputArray
	 * @param target 
	 * @return
	 */
	private int threeSumClosest(int[] nums, int target) {
		int result=0;
		int minDiff=Integer.MAX_VALUE;
		
		Arrays.sort(nums);
		int length=nums.length;
		for(int i=0;i<length-2;i++){
			int left=i+1;
			int right=length-1;
			
			while(left<right){
				int sum=nums[i]+nums[left]+nums[right];
				int tempDiff=Math.abs(sum-target);
				if(tempDiff<minDiff){
					minDiff=tempDiff;
					result=sum;
				}else if(sum>target){
					right--;
				}else if(sum<target){
					left++;
				}else{
					return result;
				}
			}
		}
		return result;
	}

}
