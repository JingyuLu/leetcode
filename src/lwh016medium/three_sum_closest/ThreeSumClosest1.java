package lwh016medium.three_sum_closest;

import java.util.Arrays;

/*
Given an array S of n integers, find three integers in S such that the sum is closest to a given number, target. Return the sum of the three integers. You may assume that each input would have exactly one solution.

    For example, given array S = {-1 2 1 -4}, and target = 1.

    The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).
 */
public class ThreeSumClosest1 {

	public static void main(String[] args) {
		ThreeSumClosest1 tsc=new ThreeSumClosest1();
		
		int[] inputArray={-55,-24,-18,-11,-7,-3,4,5,6,9,11,23,33};
		int target=0;
		
		int result=tsc.threeSumClosest(inputArray,target);
		System.out.println(result);
	}

	/**
	 * @param inputArray
	 * @param target 
	 * @return
	 */
	private int threeSumClosest(int[] nums, int target) {
		
		Arrays.sort(nums);
		int result=0;
		int counter=Integer.MAX_VALUE;
		if(nums.length==0){
			return result;
		}

		for(int i=0;i<nums.length-2;i++){
			int tempI=nums[i];
			for(int j=i+1;j<nums.length;j++){
				int tempJ=nums[j];
				for(int k=j+1;k<nums.length;k++){
					int tempK=nums[k];
					int tempSum=tempI+tempJ+tempK;
					int compare=target-tempSum;
					System.out.println(nums[i]+" "+nums[j]+" "+nums[k]+" "+tempSum+" "+compare);
					if(Math.abs(compare)<counter){
						counter=compare;
						result=nums[i]+nums[j]+nums[k];
					}
					
				}
			}
		}
		
		return result;
	}

}
