package lwh021easy.merge_two_sorted_lists;

/*
 * �ں���������list
Merge two sorted linked lists and return it as a new list. The new list should be made by splicing together the nodes of the first two lists.

Example:

Input: [1,2,4], [1,3,4]
Output: 1->1->2->3->4->4
 */
public class MergeSortedList {

	
	
	public ListNode mergeTwoLists(ListNode l1, ListNode l2) {

		if(l1==null){
			return l2;
		}else if(l2==null){
			return l1;
		}else if(l1.val<l2.val){
			l1.next=mergeTwoLists(l1.next, l2);
//			System.out.println("==============");
			return l1;
		}else{
			l2.next=mergeTwoLists(l1, l2.next);
//			System.out.println("==============");
			return l2;
		}
    }

	public static void main(String[] args) {
		MergeSortedList msl=new MergeSortedList();
		int[] array1={1,2,4};
		int[] array2={1,3,4};
		ListNode ln1=arrayToLinkedList(array1);
		ListNode ln2=arrayToLinkedList(array2);
		
		while(ln1.next!=null){
			System.out.println(ln1.val+" ");
		}
		
		ListNode ln=msl.mergeTwoLists(ln1, ln2);
//		System.out.println(ln1);
//		while(ln1.next!=null){
//			System.out.print(ln1.val+" ");
//		}
	}

	/**
	 * @param array1
	 * @return
	 */
	private static ListNode arrayToLinkedList(int[] array1) {
//		System.out.println("==============");
		ListNode temp=new ListNode(0);
		for(int i=0;i<array1.length;i++){
			temp.next=new ListNode(array1[i]);
//			System.out.println(temp.next);
			temp=temp.next;
//			System.out.println(temp);
		}
//		System.out.println(temp);
		return temp;
	}
}

class ListNode{
	int val;
	ListNode next;

	public ListNode(int value) {
		this.val=value;
	}
}
