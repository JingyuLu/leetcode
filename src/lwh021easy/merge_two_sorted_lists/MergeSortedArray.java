package lwh021easy.merge_two_sorted_lists;

import java.util.ArrayList;
import java.util.List;

/*
Merge two sorted linked lists and return it as a new list. The new list should be made by splicing together the nodes of the first two lists.

Example:

Input: [1,2,4], [1,3,4]
Output: 1->1->2->3->4->4
 */
public class MergeSortedArray {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MergeSortedArray ml=new MergeSortedArray();
		int[] input1={1,2,4};
		int[] input2={1,3,4};
		
		int[] result=ml.mergeTwoSortedList(input1,input2);
		
		for(int tempNum:result){
			System.out.print(tempNum+" ");
		}
	}

	/**
	 * @param input1
	 * @param input2
	 * @return
	 */
	private int[] mergeTwoSortedList(int[] input1, int[] input2) {
		
		int length1=input1.length;
		int length2=input2.length;
		int[] result=new int[length1+length2];
		int i=length1-1,j=length2-1,index=length1+length2-1;
		
		while(i>=0&&j>=0){
			//处理两个数组等长部分
			if(input1[i]==input2[j]){
				result[index]=input1[i];
				i--;
				index--;
				result[index]=input2[j];
				j--;
				index--;
			}else if(input1[i]>input2[j]){
				result[index]=input1[i];
				i--;
				index--;
			}else{
				result[index]=input1[j];
				j--;
				index--;
			}
		}
		
		//处理较长的数组多出的部分
		while(i>=0){
			result[index]=input1[i];
			i--;
			index--;
		}
		while(j>=0){
			result[index]=input2[j];
			j--;
			index--;
		}
		return result;
		
	}

}
